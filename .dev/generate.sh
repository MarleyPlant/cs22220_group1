#!/bin/sh
FIRSTSPRINTDATE=$(date -d "2024-01-30" '+%s')
STARTDATE=$(date -d "this tuesday" +%d/%m/%Y)
ENDDATE=$(date -d "next tuesday" +%d/%m/%Y)
SPRINTNUMBER=$(( ( $(date -d "this tuesday" '+%s') - FIRSTSPRINTDATE )/(60*60*24) / 7 ))

echo "Start date: $STARTDATE"
echo "End date: $ENDDATE"
echo "Sprint number: $SPRINTNUMBER"



#define the template.
d=./config/blogs/map166/
AUTHOR=map166
FILENAME="$(date -d "this tuesday" +%Y-%m-%d)_blog-$SPRINTNUMBER.txt"

RENDER="
Author: $AUTHOR
Week: $SPRINTNUMBER ($STARTDATE-$ENDDATE)

Assigned actions
Hours: 0
==============


Other project work
Hours: 0
================

================    
Total hours: 0

"

echo "Checking if file exists"
echo "File name: $FILENAME"
if test -f "$d/$FILENAME" 
then
    echo "File exists."
else
    echo "File does not exist."
    echo $(echo "$RENDER" > $d/$FILENAME)
fi

echo ""