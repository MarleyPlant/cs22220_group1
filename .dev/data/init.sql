CREATE TABLE "Players" (
  "id" INT GENERATED BY DEFAULT AS IDENTITY UNIQUE PRIMARY KEY,
  "username" varchar
);

CREATE TABLE "Questions" (
  "id" INT GENERATED BY DEFAULT AS IDENTITY UNIQUE PRIMARY KEY,
  "questionText" text,
  "imagePath" varchar,
  "questionType" varchar
);

CREATE TABLE "Sessions" (
  "id" INT GENERATED BY DEFAULT AS IDENTITY UNIQUE PRIMARY KEY,
  "hostID" INT REFERENCES "Players" ("id"),
  "noOfConnectedUsers" int,
  "timeLimit" float,
  "currentRound" int DEFAULT 0, 
  "currentQuestion" INT REFERENCES "Questions" ("id"),
  "isAnswering" INT REFERENCES "Players" ("id"),
  "hasEnded" BOOLEAN DEFAULT FALSE
);

CREATE TABLE "SessionPlayers" (
  "id" INT GENERATED BY DEFAULT AS IDENTITY UNIQUE PRIMARY KEY,
  "sessionID" INT REFERENCES "Sessions" ("id") ON DELETE CASCADE,
  "playerID" int REFERENCES "Players" ("id")
);


CREATE TABLE "Answers" (
  "id" INT GENERATED BY DEFAULT AS IDENTITY UNIQUE PRIMARY KEY,
  "questionID" int REFERENCES "Questions" ("id"),
  "answerText" text
);

CREATE TABLE "Scores" (
  "sessionID" int REFERENCES "Sessions" ("id"),
  "playerID" int REFERENCES "Players" ("id"),
  "modified" timestamp DEFAULT NOW(),
  "score" int,
  PRIMARY KEY ("sessionID", "playerID")
);

CREATE TABLE "Teams" (
  "id" INT GENERATED BY DEFAULT AS IDENTITY UNIQUE PRIMARY KEY,
  "fUID" int,
  "sUID" int,
  "sessionID" int
);

CREATE TABLE "Session_Questions" (
  "Session_sessionID" varchar,
  "Questions_sessionID" varchar,
  PRIMARY KEY ("Session_sessionID", "Questions_sessionID")
);

ALTER TABLE "Players" ADD FOREIGN KEY ("id") REFERENCES "Teams" ("fUID");

ALTER TABLE "Players" ADD FOREIGN KEY ("id") REFERENCES "Teams" ("sUID");
