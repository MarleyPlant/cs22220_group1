# **House Of Games** 

CS22220 Group Project - Group 1
The Aim Of The Project is to create an online quiz game that
follows the same rules as [Richard Osmans House Of Games](https://en.wikipedia.org/wiki/Richard_Osman%27s_House_of_Games)

You can find a video demo [here](https://youtu.be/lAiOg5bFs-M)

<br />

## Planning Documents Table
| Name                   | Spec           | Location                                 | Status |
| ---------------------- | -------------- | ---------------------------------------- | ------ |
| UI Prototype           | [blackboard]() | [/docs/uiprotype](/docs/uiprototype)                     | ✔️      |
| Architectural Overview | [blackboard]() | [/docs/architecturaloverview](/docs/ATP2) | ✔️      |
| ATP3 Docs              | [blackboard]() | [/docs/ATP3](/docs/ATP3)                 | ✔️      |

<br />

## Project Requirements

## User Stories
So far, we have released US1, US2, US3, US6, US10, with US17, US18, US19 partially meeting the requirements.


### Standards
| Name                                 | Blackboard                                                                             | Item  |
| ------------------------------------ | -------------------------------------------------------------------------------------- | ----- |
| Documentation                        | [link](https://blackboard.aber.ac.uk/ultra/courses/_46442_1/outline/file/_2684373_1)   | seqa2 |
| Project Management                   | [link](https://blackboard.aber.ac.uk/ultra/courses/_46442_1/outline/file/_2684374_1)   | seqa3 |
| UI Prototypes                        | [link](https://blackboard.aber.ac.uk/ultra/courses/_46442_1/outline/file/_2684375_1)   | seqa4 |
| Design                               | [link](https://blackboard.aber.ac.uk/ultra/courses/_46442_1/outline/file/_2684383_1)   | seqa5 |
| Sprint Reviews                       | [link](https://blackboard.aber.ac.uk/ultra/courses/_46442_1/outline/file/_2684378_1)   | seqa7 |
| Configuration & Operating Procedures | [link](https://blackboard.aber.ac.uk/ultra/courses/_46442_1/outline/file/_2684379_1)   | seqa8 |

<br />

### 📐 **Code Style**
The project should follow strict guildlines for the coding style, as set in [seqa8](https://blackboard.aber.ac.uk/ultra/courses/_46442_1/outline/file/_2684379_1)().

<br />

## Development

### Start Docker Services
In order to get started with the repository, simply [install docker]() and [Node.js]() run the run the following commands **from the root of the repository**.

```bash
npm install
npm start
```

Once services are installed and running you should be able to access the services with

| Service  | Hostname |
| -------  | -------- |
| Frontend | [localhost:8000](http://localhost:8000) |
| Backend  | [localhost:8001](http://localhost:8001) |
| Database | [localhost:5432](http://localhost:5432) |
| Adminer  | [localhost:8080](http://localhost:8080) |


### Testing
In order to run unit tests for the application you can run the following

```bash
npm run test
```

#### Manual Testing
Testing will be done manually by each member of the group (with result recorded in a "System Specification" document in /docs/ATP3). PHP scripts will be tested with PHPUnit aswell. These tests can be found in src/backend/tests/inc/controllers. These are PHP Unit tests using CRUD.

### Linting
The lint command works the same as the test command, it runs inside the docker enviroment.
and is responsible for ensuring that the codebases both meet the requirements.

#### Docker
```bash
npm run lint
```

## Resources


<br />

## 📁 **Repository Structure**

| Item            | Location        | README                            | Purpose                            |
| --------------- | --------------- | --------------------------------- | ---------------------------------- |
| Frontend Source | /src/frontend   | [HERE](/src/frontend/)   | Frontend HTML Source               |
| Backend Source  | /src/backend    | [HERE](/src/backend/)    | Backend PHP API Source             |
| Blogs           | /config/blogs   | [HERE](/config/blogs/README.md)   | Keep Track Of Development Progress |
| Minutes         | /config/minutes | N/A                               | Keep Track Of Meetings             |
| Development     | /dev/           | [HERE](/dev/README.md)            | Folder for in progress work        |

<br />

## 🧑‍💼 **Contributors**

| Name             | Blogs                         | Social                                                                                                                                                                                                                                                                                                                | University Email                   | Discord      |
| ---------------- | ----------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------- | ------------ |
| Marley Plant     | [HERE](/config/blogs/map166/) | ![GitLab](https://img.shields.io/badge/Marley%20Plant-Marley?style=flat-square&logo=gitlab&color=%23554488&link=https%3A%2F%2Fgitlab.com%2FMarleyPlant) ![Static Badge](https://img.shields.io/badge/MarleyPlant-Github?style=flat&logo=github&logoColor=white&color=333&link=https%3A%2F%2Fgithub.com%2FMarleyPlant) | [map166](mailto:map166@aber.ac.uk) | marleyjplant |
| Kacper Podlewski | [HERE](/config/blogs/map166/) | ![Gitlab](https://img.shields.io/badge/Kacper%20Podlewski-Gitlab?style=flat-square&logo=gitlab&color=%23554488)                                                                                                                                                                                                       | [kap62](mailto:kap62@aber.ac.uk)       | htuos_ |
| Joseph Asante    | [HERE](/config/blogs/map166/) | ![GitLab](https://img.shields.io/badge/Joseph%20Asante-Gitlab?style=flat-square&logo=gitlab&color=%23554488)                                                                                                                                                                                                         | [joa51](mailto:joa51@aber.ac.uk)       | ############ |
| Jakub Grzebinoga | [HERE](/config/blogs/map166/) | ![GitLab](https://img.shields.io/badge/Jakub%20Grzebinoga-Gitlab?style=flat-square&logo=gitlab&color=%23554488)                                                                                                                                                                                                       | [jag88](mailto:jag88@aber.ac.uk)       | n/a |
| Brandon Kelleher | [HERE](/config/blogs/brk14/) | ![GitLab](https://img.shields.io/badge/Brandon%20Kelleher-Gitlab?style=flat-square&logo=gitlab&color=%23554488)                                                                                                                                                                                                       | [brk14](mailto:brk14@aber.ac.uk)       | brandonkelleher |
| Ahnaf Tahmid     | [HERE](/config/blogs/map166/) | ![GitLab](https://img.shields.io/badge/Ahnaf%20Tahmid-Gitlab?style=flat-square&logo=gitlab&color=%23554488)                                                                                                                                                                                                           | [aht6](mailto:aht6@aber.ac.uk)       | liesofp |

<br />

## 💻 **TECHNOLOGIES**
[![Docker](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Docker-2496ED?style&#x3D;for-the-badge&amp;logo&#x3D;Docker&amp;logoColor&#x3D;white)]()
[![PostgreSQL](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;PostgreSQL-4169E1?style&#x3D;for-the-badge&amp;logo&#x3D;PostgreSQL&amp;logoColor&#x3D;white)]()
[![Adminer](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Adminer-34567C?style&#x3D;for-the-badge&amp;logo&#x3D;Adminer&amp;logoColor&#x3D;white)]()

[![Figma](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Figma-F24E1E?style&#x3D;for-the-badge&amp;logo&#x3D;Figma&amp;logoColor&#x3D;white)]()
[![Visual Studio Code](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Visual%20Studio%20Code-007ACC?style&#x3D;for-the-badge&amp;logo&#x3D;Visual%20Studio%20Code&amp;logoColor&#x3D;white)]()

[![Bootstrap](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Bootstrap-7952B3?style&#x3D;for-the-badge&amp;logo&#x3D;Bootstrap&amp;logoColor&#x3D;white)]()
[![Material Design](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Material%20Design-757575?style&#x3D;for-the-badge&amp;logo&#x3D;Material%20Design&amp;logoColor&#x3D;white)]()
[![Font Awesome](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Font%20Awesome-528DD7?style&#x3D;for-the-badge&amp;logo&#x3D;Font%20Awesome&amp;logoColor&#x3D;white)]()

[![npm](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;npm-CB3837?style&#x3D;for-the-badge&amp;logo&#x3D;npm&amp;logoColor&#x3D;white)]()
[![Composer](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Composer-885630?style&#x3D;for-the-badge&amp;logo&#x3D;Composer&amp;logoColor&#x3D;white)]()
[![ESLint](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;ESLint-4B32C3?style&#x3D;for-the-badge&amp;logo&#x3D;ESLint&amp;logoColor&#x3D;white)]()

[![React](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;React-61DAFB?style&#x3D;for-the-badge&amp;logo&#x3D;React&amp;logoColor&#x3D;white)]()
[![React Router](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;React%20Router-CA4245?style&#x3D;for-the-badge&amp;logo&#x3D;React%20Router&amp;logoColor&#x3D;white)]()

[![PHP](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;PHP-777BB4?style&#x3D;for-the-badge&amp;logo&#x3D;PHP&amp;logoColor&#x3D;white)]()
[![HTML5](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;HTML5-E34F26?style&#x3D;for-the-badge&amp;logo&#x3D;HTML5&amp;logoColor&#x3D;white)]()
[![CSS3](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;CSS3-1572B6?style&#x3D;for-the-badge&amp;logo&#x3D;CSS3&amp;logoColor&#x3D;white)]()
[![JavaScript](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;JavaScript-F7DF1E?style&#x3D;for-the-badge&amp;logo&#x3D;JavaScript&amp;logoColor&#x3D;white)]()
[![Sass](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;Sass-CC6699?style&#x3D;for-the-badge&amp;logo&#x3D;Sass&amp;logoColor&#x3D;white)]()
[![JSON](https:&#x2F;&#x2F;img.shields.io&#x2F;badge&#x2F;JSON-000000?style&#x3D;for-the-badge&amp;logo&#x3D;JSON&amp;logoColor&#x3D;white)]()

### 📖 **References**
- [Figma UI Designs](https://www.figma.com/file/rO8hKAdtOgB8pRgjWRkhjz/House-of-Games?type=design&node-id=177%3A8639&mode=design&t=DPdlG4ITfwapnm0p-1)
- [React Toastify](https://github.com/fkhadra/react-toastify)

<br />