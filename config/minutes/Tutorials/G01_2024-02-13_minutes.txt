Project: Group 1 CS22220
Meeting: Weekly Project Meeting 
Persons present: All project members except Joseph
Place of meeting: LL-C40
Author: Marley Plant
Date of minutes: 13th February 
Version: 1.0

Circulation list: 

Kelleher, Brandon: brk14	
Grzebinoga, Jakub: jag88	
Plant, Marley: map166	
Asante, Joseph: joa51	
Podlewski, Kacper: kap62	
Tahmid, Ahnaf: aht6	
Muhammad Aslam: mua19 (Scrum Master)

Matters arising 
===============
===============

Review
---------------
---------------


Retrospective
---------------
---------------


Other matters
---------------
---------------


Sprint planning
---------------
Action: All

8. Story DS2 (Architectural design)
Sprint 3:
    Architectural Overview (Marley)
    Page structure of the site (Need To Decide Wednesday Meeting)
    Mapping from user stories to pages
    Database Structure
        User Logins and passwords (Brandon + Jakub)
        Questions and Answers (Anhaf + Marley)
        Scores (Joseph)
        Sessions (Kacper)
    Start DETAILS OF EACH PAGE (everyone)

    



Other Business
---------------

1. Spreadsheet With Hours | Update Spreadsheet with everyones Hours

2. Reupload Powerpoint

---------------

