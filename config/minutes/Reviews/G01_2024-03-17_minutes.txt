Project: Group 1 (CS22210/T/1/01)
Meeting: Review Meeting
Persons present:All
Place of meeting: Discord
Author: Kacper Podlewski
Date of minutes: 17/03/2024
Version: 1.0

Circulation list: 

Kelleher, Brandon: brk14
Grzebinoga, Jakub: jag88
Plant, Marley: map166
Asante, Joseph: joa51
Podlewski, Kacper: kap62
Tahmid, Ahnaf: aht6

Matters arising 
===============

Review
---------------

Details document - All happy, passed review

Systems specification document - All happy, passed review

Story 1.1 - Passed Review
Story 1.2 - Passed Review
Story 1.3 - Passed Review
Story 1.4 - Passed Review 
Story 1.5 - Passed Review
Story 17.1 - Passed Review
Story 19.1 - Passed Review
Story 19.2 - Passed Review

Readmes - All happy, passed review

Site to be put together 

---------------

Retrospective
---------------

Ahnaf - Tasks have been changed too much, weren't clear on how to tackle submission. Mismanaged the submission

Brandon - Week has been a mess, lack of understanding of the ATP and what work needs to be done and submitted. 

Jakub - Week went poorly, meetings he established weren't attended so work wasn't completed, mismanaged time. Unhappy with time management.

Joseph - Week went okay.

Kacper - Messy week for us, not much work completed, but people need to stop bickering / arguing. 

Marley - All good. 


---------------
Other matters
---------------

Merging - MAP166 - Sprint7 - optionaldocker - .dev folder / dockerocmpose / backend / Blogs

Jakub - Readme files in parent directory , readme in /src/backend and /src/frontend and empty directory in src 

Frontend House of games folder go into HTML, rest of it goes into .dev

---------------

Sprint Planning
===============
===============

Other Business
---------------
---------------
===============