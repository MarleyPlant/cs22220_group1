Author: joa51
Sprint: 10 (23/04/2024 - 30/04/2024) 

Assigned actions
==============

Action 1: Do the system specifications documentation - Test the functions of the website and record what happened. Do the UI Prototypes. 

I have tested the functions of the website and recorded what happened. 
I also reported the failed tests to my group members so that they could take a look at them and fix anything that was not working.
The UI Prototype is currently in progress.

Other project work
================

Hours: 4
