Author: JAG88
Sprint: 3 (13/02/2024 - 20/02/2024)

Assigned actions
==============
14/02/2024 - Attended meeting (1hr)
16/02/2024 - Created Database structure for sessions (Met with jag88 to do the database structure for sessions using diagramdb) (2hr)

Other project work
================
17/02/2024 - Made UML sequence diagram for new session handling (2hr)
20/02/2024 - Made UML sequence diagram for existing session handling based on UML sequence diagram for new session handling (30min)

Total hours: 5.5 hrs

