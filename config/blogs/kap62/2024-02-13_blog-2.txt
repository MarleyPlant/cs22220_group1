Author: kap62
Sprint: 2 (06/02/2024 - 13/02/2024)

Assigned actions
========================

Action 1: Adjust navigation bar and add Round counter to all pages

Completed as required, introduced round counter as well as adjusted the navbar on all pages

Hours : 3
============

Total hours : 3
