# **House Of Games** - Developer Blog
As part of the requirements for the module we are required to keep a blog detailing each sprint.
this should follow the guidelines outlined in this document

<br />

## Filenames
each developer should have their own folder which references their university email address.
within each folder you will then have a blog for each development sprint. 
You should create you blog post on the first day of the sprint if possible
`{YYYY}-{MM}-{DD}-blog{n}.txt` where n refers to the sprint number.

<br />

## Template
```
Author: {username}
Week: 1 (06/02/2024-12/02/2024)

Assigned actions
Hours: {h}

==============


=============-


Other project work
Hours: {h}
================



Hours: {h}
Total hours: {th}
```

<br />


## Assigned Actions
During each sprint the developer is given a number of assigned actions to be completed in that week.
For each of these actions you must add an entry within your blogpost.

```
Action {n}: 


```