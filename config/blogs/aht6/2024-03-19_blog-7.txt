Author: aht6
Sprint: 7 (12/03/2024 - 19/03/2024) 

Assigned actions
Hours: 7.5
==============
Action: PHP Unit Tests/Manual Tests
Action Taken: I assisted Marley and Brandon to test PHP and complete the testing table and format document. 2 Hours

Action: PHP writing the code and for API and UNIT test
Action Taken: I worked with Marley and Brandon to go over written code to make sure it running and functional. 4 Hour

Action: Add action/js to forms for the HTML files for the php to be able to interact for US1
Action Taken: Went over all of the HTML and made changes according to US1 requirements. 2 Hours

Action: Implementation of PHP and the HTML to be able to call for data and store data.
Action Taken: Assisting and testing ways of implementing the php and html to function together to carry out the task of US1 and US19 
2 Hours

Other project work
Hours: 10
================
Assisting Marley with the creation of the PHP routes/API. 1 Hour
Compiling of HTML and PHP code. 3 Hours

================    
Total hours: 14 Hours 
