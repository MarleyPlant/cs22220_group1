Author: Aht6

Sprint: 2 (06/02/2024 - 13/02/2024)

**Assigned actions**
====================

**Action 1**: Make adjustments to final score page of the UI Prototype/Make appropriate changes to powerpoint according to changes made.

Action taken:

Used more of space available within the web page by adjusting the logo and web title, re-arranged the scoreboard and added medals for top three. Carried out on figma.

Updated the powerpoint with the new page screenshot.


Hours: 2.0

**Other project work**
======================

Looking through the other pages done by group members suggesting improvements and changes that are needed. As well as communicating any changes made to the group via discord.

Hours: 0.5

Total hours: 2.5 Hours
