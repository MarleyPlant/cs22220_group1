| Item         | Configuration Reference | Name          | Location                         | Status |
| ------------ | ----------------------- | ------------- | -------------------------------- | ------ |
| UI Prototype | UIPrototype_1           | UI Prototypes | /docs/ATP1/prototype.pptx | Release |
| Planning Spreadsheet | Planning_1      | planning.xlsx | /config/planning.xlsx | Release |
| Architectural Designs | Architectural-1      | Architectural Designs.pdf | /docs/ATP2 | Release |
| Design Specification Doc | DesignSpecification1      | Design Specification Document | /docs/ATP3 | Release |
| Systems Specification Doc | SystemsSpecification1      | Systems Specification Document | /docs/ATP3 | Release |
| Systems Specification Doc Final | SystemsSpecification_updated      | Systems Specification Document | /docs/Final Submission | Release |
| Design Specification Doc Final | DesignSpecifications_updated     | Design Specification Document | /docs/Final Submission | Release |
| Final Report | FinalReport1      | Final Report | /docs/Final Submission | Release |
| Maintenance Manual | MaintenanceManual1      | Maintenance Manual | /docs/Final Submission | Release |
| UI Prototypes Final | UIPrototypes_updated      | UI Prototypes | /docs/Final Submission | Release |
