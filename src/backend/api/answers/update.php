<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("id", $_GET)) {
    echo json_encode($db->controllers['answers']->update($_GET));
} else {
    echo json_encode(["error" => "Invalid request"]);
}
