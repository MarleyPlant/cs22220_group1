<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("answerText", $_GET)) {
    if (array_key_exists("answerText", $_GET)) {
        $answerText = $_GET['answerText'];
    }
    if (array_key_exists('questionID', $_GET)) {
        $questionID = $_GET['questionID'];
    }

    $result = $db->controllers['answers']->add(
        [
            'answerText' => $answerText,
            'questionID' => $questionID,
        ]
    );

    echo json_encode(['success' => $result]);
} else {
    echo json_encode(["error" => "Invalid request"]);
}
