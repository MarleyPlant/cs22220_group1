<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("session", $_GET) && array_key_exists("player", $_GET)) {
    $db->controllers['sessions']->addPlayer([
        'sessionID' => $_GET['session'],
        'playerID' => $_GET['player']
    ]);

    echo json_encode(['success' => true]);
} else {
    echo json_encode(["error" => "Invalid request"]);
}
