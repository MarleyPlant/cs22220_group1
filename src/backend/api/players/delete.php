<?php

header('Content-Type: application/json; charset=utf-8');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("username", $_GET)) {
    $timeLimit = 0;

    echo json_encode($db->controllers['players']->delete(
        [
        'username' => $_GET['username'],
        ]
    ));
} else {
    echo json_encode(["error" => "Invalid request"]);
}
