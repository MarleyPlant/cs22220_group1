This file should be used to plan out the API Routes that are required for the application
a ? before a parameter means that the parameter is optional

# [Questions](https://gitlab.aber.ac.uk/jag88/cs22220_group1/-/tree/main/inc/controllers/questions.php)

| Route             | Parameters                                           | Statements   |
| ----------------- | ---------------------------------------------------- | ------------ |
| /question         | id                                                   | getSingle    |
| /questions        | ?id       , ?questionType                            | getAll       |
| /question/delete  | id                                                   | DeleteSingle |
| /questions/add    | questionText, ?imagePath, questionType               | addNew       |
| /questions/update | questionID, ?questionText, ?imagePath, ?questionType | updateSingle |


# Answers

| Route           | Parameters                       | Statements    |
| --------------- | ---------------------------------| --------------|
| /answer         | answerID                         |  getSignle    |
| /answers        | ?questionID                      |  getAll       |
| /answer/delete  | answerID                         |  DeletSingle  |
| /answers/add    | questionID, answerText           |  addNew       |
| /answers/update | answerID, questionID, answerText |  updateSingle |


# Players

| Route             | Parameters   | Statements     |
| ----------------- | ------------ | -------------- |
| /player           | id          | getSingle      |
| /players          | None         | getAll         |
| /player/delete    | id          | DeleteSingle   |
| /player/add       | username     | addNew         |
| /player/update    | id, username| updateSingle   |

# Session

| Route             | Parameters                                       | Statements   |
| ----------------- | ------------------------------------------------ | ------------ |
| /session          | sessionID                                        | getSingle    |
| /sessions         | None                                             | getAll       |
| /session/delete   | sessionID                                        | DeleteSingle |
| /sessions/add     | hostID, noOfConnectedUsers, timeLimit            | addNew       |
| /sessions/update  | sessionID, hostID, noOfConnectedUsers, timeLimit | updateSingle |


# Scores

| Route             | Parameters                                 | Statements     |
| ----------------- | ------------------------------------------ | -------------- |
| /score            | scoreID                                    | getSingle      |
| /scores           | ?uID, ?sessionID                           | getAll         |
| /score/delete     | scoreID                                    | DeleteSingle   |
| /scores/add       | uID, sessionID, date, time, score          | addNew         |
| /scores/update    | scoreID, uID, sessionID, date, time, score | updateSingle   |

# Teams

| Route             | Parameters                         | Statements   |
| ----------------- | ---------------------------------- | ------------ |
| /team             | teamID                             | getSingle    |
| /teams            | ?fUID, ?sUID, ?sessionID           | getAll       |
| /team/delete      | teamID                             | DeleteSingle |
| /teams/add        | fUID, sUID, sessionID              | addNew       |
| /teams/update     | teamID, fUID, sUID, sessionID      | updateSingle |

