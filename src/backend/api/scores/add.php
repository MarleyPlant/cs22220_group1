<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("sessionID", $_GET) && array_key_exists("playerID", $_GET)) {
    $result = $db->controllers['scores']->add(
        $_GET
    );

    echo json_encode(['success' => $result]);
} else {
    echo json_encode(["error" => "Invalid request"]);
}
