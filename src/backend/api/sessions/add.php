<?php

header('Content-Type: application/json; charset=utf-8;');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("hostID", $_GET)) {
    $timeLimit = 0;
    $numberOfConnectedUsers = 0;
    if (array_key_exists("noOfConnectedUsers", $_GET)) {
        $numberOfConnectedUsers = $_GET['noOfConnectedUsers'];
    }
    if (array_key_exists('timelimit', $_GET)) {
        $timeLimit = $_GET['timelimit'];
    }
    $gameid = $db->controllers['sessions']->add(
        [
            'hostID' => $_GET['hostID'],
            'noOfConnectedUsers' => $numberOfConnectedUsers,
            'timeLimit' => $timeLimit,        ]
    );
    echo json_encode(["gameid" => $gameid]);
} else {
    echo json_encode(["error" => "Invalid request"]);
}
