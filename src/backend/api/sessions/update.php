<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("id", $_GET)) {
    if (array_key_exists("currentRound", $_GET)) {
        $_GET['currentRound'] = intval($_GET['currentRound']);
    }

    echo json_encode($db->controllers['sessions']->update($_GET));
} else {
    echo json_encode(["error" => "Invalid request"]);
}
