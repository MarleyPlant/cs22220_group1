<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("id", $_GET)) {
    $result = $db->controllers['sessions']->getSingle(["id" => $_GET['id']]);
    if ($result === false) {
        echo json_encode(["error" => true]);
        die();
    }
    $connectedPlayers = $db->controllers['sessions']->getConnectedPlayers(["id" => $_GET['id']]);
    $result['players'] = $connectedPlayers;
    echo json_encode($result);
} else {
    $sessions = $db->controllers['sessions']->getAll() ;
    $sessions = array_map(function ($session) {
        global $db;
        $connectedPlayers = $db->controllers['sessions']->getConnectedPlayers(["id" => $session['id']]);
        $session['players'] = $connectedPlayers;
        return $session;
    }, $sessions);
    echo json_encode($sessions);
}
