<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000');

include_once __DIR__ . "/../../inc/init.php";

$db = getDBConnection();

if (array_key_exists("questionText", $_GET)) {
    $imagePath = "";
    if (array_key_exists("questionText", $_GET)) {
        $questionText = $_GET['questionText'];
    }
    if (array_key_exists('imagePath', $_GET)) {
        $imagePath = $_GET['imagePath'];
    }
    if (array_key_exists('questionType', $_GET)) {
        $questionType = $_GET['questionType'];
    }

    $result = $db->controllers['questions']->add(
        [
            'questionText' => $questionText,
            'imagePath' => $imagePath,
            'questionType' => $questionType,
        ]
    );

    echo json_encode(['success' => $result]);
} else {
    echo json_encode(["error" => "Invalid request"]);
}//end if
