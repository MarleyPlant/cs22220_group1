<?php

header('Content-Type: application/json; charset=utf-8');
header('Access-Control-Allow-Origin: http://localhost:8000 always', true);

$json = file_get_contents('php://input');
$json = json_decode($json);
$json = $json->output;

// decode the json data
$dir    = __DIR__ . '/../../uploads/';
$path = realpath($dir);
$files1 = scandir($path);
$filename = (count($files1) - 2) . ".png";
$output_file = realpath($path) . "/" . $filename;

if (!$json) {
    echo json_encode([
        "status" => "error",
        "message" => "No data found",
    ]);

    die();
}

if (file_put_contents($output_file, base64_decode($json))) {
    echo json_encode([
        "status" => "success",
        "message" => "File uploaded successfully",
        "path" => "/uploads/" . $filename,
    ]);
}
