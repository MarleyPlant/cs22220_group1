<?php
use function PHPUnit\Framework\assertEquals;

// phpcs:ignoreFile
include_once __DIR__ . "/../baseControllerCase.php";

final class sessionTest extends ControllerTestCase
{
    public array $exampleData;
    public string $playerID;
    public function setUp(): void
    {
        $this->db = getDBConnection();
        $this->controller = new SessionController($this->db);
        $this->playersController = new PlayersController($this->db);
        $this->playerID = $this->playersController->add([
            "username" => "example",
        ]);
        $this->exampleData = [
            "hostID" => $this->playerID,
            "noOfConnectedUsers" => 10,
            "timeLimit" => 100
        ];
    }


    public function testCRUD(): void
    {
        $this->CanAddDelete($this->controller);
        $this->CanAddDelete($this->controller);
        $this->CanGet($this->controller);
        $this->CanGetAll($this->controller);
    }


    // public function testCanUpdate(): void
    // {
    //     $testDataID = $this->add($this->controller);

    //     $this->controller->update(["id" => $testDataID, "noOfConnectedUsers" => 20]);

    //     assertEquals($this->controller->getSingle([
    //         "id" => $testDataID
    //     ])["noOfConnectedUsers"], 20);

    //     $this->delete($this->controller, $testDataID);

    // }
}