<?php
// phpcs:ignoreFile
include_once __DIR__ . "/../baseControllerCase.php";

final class playersTest extends ControllerTestCase
{
    public array $exampleData;
    public function setUp(): void
    {
        $this->db = getDBConnection();
        $this->controller = new PlayersController($this->db);
        $this->exampleData = [
            "username" => "example",
        ];
    }


    public function testCRUD() : void {
        
        $this->CanAddDelete($this->controller);
        $this->CanAddDelete($this->controller);
        $this->CanGet($this->controller);
        $this->CanGetAll($this->controller);
    }
}
