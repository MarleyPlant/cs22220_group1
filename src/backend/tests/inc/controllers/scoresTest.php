<?php
// phpcs:ignoreFile
include_once __DIR__ . "/../baseControllerCase.php";

final class scoresTest extends ControllerTestCase
{
    public array $exampleData;
    public string $playerID;
    public string $sessionID;

    public function setUp(): void
    {
        $this->db = getDBConnection();
        $this->controller = new ScoresController($this->db);
        $this->playersController = new PlayersController($this->db);
        $this->sessionsController = new SessionController($this->db);
        $this->playerID = $this->playersController->add([
            "username" => "example",
        ]);
        $this->sessionID = $this->sessionsController->add([
            "hostID" => $this->playerID,
            "noOfConnectedUsers" => 10,
            "timeLimit" => 100
        ]);
        $this->exampleData = [
            "playerID" => $this->playerID,
            "sessionID" => $this->sessionID,
            "score" => 100,
        ];
    }

    public function tearDown(): void
    {
        $this->controller->delete([
            "playerID" => $this->playerID,
            "sessionID" => $this->sessionID,
        ]);
        $this->sessionsController->delete([
            "id" => $this->sessionID,
        ]);
        $this->playersController->delete([
            "id" => $this->playerID,
        ]);

    }

    public function CanGet($controller): void
    {
        $testDataID = $this->add($controller);
        $result = $controller->getScores([
            "playerID" => $this->playerID,
            "sessionID" => $this->sessionID
        ]);
        $this->delete($controller, $testDataID);
        $this->libResult($result[0], $this->exampleData);
    }


    function delete($controller, $id)
    {
        return $controller->delete([
            "playerID" => $this->playerID,
            "sessionID" => $this->sessionID,
        ]);
    }

    public function testCRUD(): void
    {
        $this->CanAddDelete($this->controller);
        $this->CanAddDelete($this->controller);
        $this->CanGet($this->controller);
    }


}
