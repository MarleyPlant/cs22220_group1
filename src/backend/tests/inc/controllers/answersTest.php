<?php
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertTrue;
// phpcs:ignoreFile
include_once __DIR__ . "/../baseControllerCase.php";

final class answersTest extends ControllerTestCase
{
    public array $exampleData;
    public function setUp(): void
    {
        $this->db = getDBConnection();
        $this->controller = new AnswersController($this->db);
        $questionsController = new QuestionsController($this->db);
        $id = $questionsController->add([
            "questionText" => "This is a test question",
            "questionType" => "text",
            "imagePath" => "test",
            "sessionID" => 1,
        ]);
        $this->exampleData = [
            "questionID" => $id,
            "answerText" => "This is a test answer",
        ];
    }

    public function tearDown(): void
    {
        $querydata = "'This is a test question'"; // phpcs:ignore
        $this->db->db->query('DELETE FROM "Questions" WHERE "questionText" = ' . $querydata);
    }


    public function testCRUD() : void {
        
        $this->CanAddDelete($this->controller);
        $this->CanAddDelete($this->controller);
        $this->CanGet($this->controller);
        $this->CanGetAll($this->controller);
    }


    public function testCanUpdate() : void
    {
        $testDataID = $this->add($this->controller);

        $isUpdateSuccess = $this->controller->update([
            "id" => $testDataID,
            "answerText" => "Example Update",
        ]);

        assertTrue($isUpdateSuccess);

        $updatedData = $this->controller->getSingle([
            "id" => $testDataID,
        ]);

        assertEquals($updatedData["answerText"], "Example Update");

        $this->delete($this->controller, $testDataID);

    }
}
