<?php
// phpcs:ignoreFile

use function PHPUnit\Framework\assertArrayHasKey;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertTrue;
include_once __DIR__ . "/../baseControllerCase.php";

final class questionsTest extends ControllerTestCase
{
    public array $exampleData;
    public function setUp(): void
    {
        $this->db = getDBConnection();
        $this->controller = new QuestionsController($this->db);
        $this->exampleData = [
            "questionText" => "What is the capital of France?",
            "questionType" => "text",
            "imagePath" => "none"
        ];
    }


    public function testCRUD() : void {
        $this->CanAddDelete($this->controller);
        $this->CanGet($this->controller);
        $this->CanGetAll($this->controller);
    }


    // public function testCanUpdate() : void
    // {
    //     $testDataID = $this->add($this->controller);

    //     $isUpdateSuccess = $this->controller->update([
    //         "id" => $testDataID,
    //         "newText" => "Example Update",
    //     ]);

    //     assertTrue($isUpdateSuccess);

    //     $updatedData = $this->controller->getSingle([
    //         "id" => $testDataID,
    //     ]);

    //     assertEquals($updatedData["questionText"], "Example Update");

    //     $this->delete($this->controller, $testDataID);

    // }
}
