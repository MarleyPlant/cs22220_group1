<?php
// phpcs:ignoreFile

use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertIsArray;
use function PHPUnit\Framework\assertArrayHasKey;
use function PHPUnit\Framework\assertNotNull;
use function PHPUnit\Framework\assertTrue;
include_once __DIR__ . "/../../inc/init.php";

class ControllerTestCase extends TestCase {

    public array $exampleData = [];
    private TableController $controller;
    private DatabaseConnection $db;

    function libResult($result) {
        assertNotNull($result);
        assertIsArray($result);
        foreach ($this->exampleData as $key => $value) {
            assertArrayHasKey($key, $result);
        }
    }
    
    public function CanAddDelete($controller): void
    {
        $result = $this->add($controller);
        assertNotNull($result);
        $result = $this->delete($controller, $result);
        assertTrue($result);
    }

    public function CanGet($controller): void
    {
        $testDataID = $this->add($controller);
        $result = $controller->getSingle([
            "id" => $testDataID,
        ]);
        $this->delete($controller, $testDataID);
        $this->libResult($result, $this->exampleData);
    }

    public function CanGetAll($controller) : void
    {
        $testDataOneID = $this->add($controller);
        $testDataTwoID = $this->add($controller);

        $result = $controller->getAll();

        assertIsArray($result);
        $this->libResults($result);
        $this->delete($controller, $testDataOneID);
        $this->delete($controller, $testDataTwoID);
    }

    function libResults($results) {
        foreach ($results as $result) {
            $this->libResult($result);
        }
    }

    function add($controller) {
        return $controller->add($this->exampleData);
    }

    function delete($controller, $id) {
        return $controller->delete([
            "id" => $id,
        ]);
    }

}
