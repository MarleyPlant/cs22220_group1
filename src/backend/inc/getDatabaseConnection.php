<?php

function getDBConnection()
{

    $connectioninfo = [
       "DBHOST" => "db.dcs.aber.ac.uk",
       "DBPORT" => 22,
       "DBNAME" => "cs22220_23_24_group01",
       "DBUSER" => "group01",
       "DBPASS" => "yPiJe3UW",
    ];

    foreach ($connectioninfo as $key => $value) {
        if (isset($_ENV[$key])) {
            $connectioninfo[$key] = $_ENV[$key];
        }
    }


    $db = new DatabaseConnection(
        $connectioninfo["DBHOST"],
        $connectioninfo["DBPORT"],
        $connectioninfo["DBUSER"],
        $connectioninfo["DBPASS"],
        $connectioninfo["DBNAME"]
    );

    return $db;
}
