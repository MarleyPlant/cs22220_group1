<?php

/**
 * Defines DatabaseConnection Class
 * is used to communicate with the database
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>

 */

/**
 * DatabaseConnection Class
 * is used to communicate with the database
 */
class DatabaseConnection
{
    public $db;
    private $host;
    private $port;
    private $username;
    private $password;
    private $database;
    private $charset;

    public $controllers;

    /**
     * DatabaseConnection Constructor
     * is used to create a new DatabaseConnection Object
     * and connect to the database
     *
     * @param mixed  $host     Database Hostname or IP.
     * @param mixed  $port     Database Port.
     * @param mixed  $username Database Username.
     * @param mixed  $password Database Password.
     * @param mixed  $database Database Name.
     * @param string $charset  Database Charset.
     *
     */
    public function __construct($host, $port, $username, $password, $database, $charset = "utf8")
    {
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
        $this->charset = $charset;

        // Attempt to connect to the database
        try {
            $this->db = new PDO('pgsql:host=' . $host . ';dbname=' . $database, $username, $password);
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), (int)$e->getCode());
        }

        // Create an array container all the TableControllers
        $this->controllers = [
            "questions" => new QuestionsController($this),
            "answers" => new AnswersController($this),
            "sessions" => new SessionController($this),
            "players" => new PlayersController($this),
            "scores" => new ScoresController($this),
        ];

        // Add All Controllers to $this->controllers
            // $this->controllers[tablenmame] = new UserController($this->db);
    }
}
