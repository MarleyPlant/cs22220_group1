<?php

/**
 * Intialize The Application, Load Database Config
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>

 *
 */

 // Load Database Config
 require __DIR__ . "/../vendor/autoload.php";
 include_once __DIR__ . "/DatabaseConnection.php";
 include_once __DIR__ . "/getDatabaseConnection.php";
