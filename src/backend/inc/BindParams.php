<?php

 /**
  * Binds an array of parameters to a PDO Statement
  *
  * @param mixed $stmt   PDO Statement.
  * @param mixed $params Array of parameters.
  *
  * @return \PDOStatement
  *
  */
function bindParams($stmt, $params)
{
    foreach ($params as $key => $value) {
        $stmt->bindParam($key, $value);
    }

    return $stmt;
}
