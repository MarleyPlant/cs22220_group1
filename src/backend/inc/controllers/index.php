<?php

/**
 * Import All Controller Classes
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>

 */

require_once __DIR__ . "/answers.php";
require_once __DIR__ . "/questions.php";
require_once __DIR__ . "/session.php";
require_once __DIR__ . "/players.php";
