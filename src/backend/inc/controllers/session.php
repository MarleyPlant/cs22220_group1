<?php

/**
 * Defines Session Controller
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>
 */


/**
 * SessionController Class
 * is used to manage the Session Table
 */
class SessionController extends TableController
{
    public $db;
    public $table = 'Sessions';

    /**
     * Initializes the TeamsController
     *
     * @param DatabaseConnection $db Instance of DatabaseConnection.
     *
     */
    public function __construct(DatabaseConnection $db)
    {

        parent::__construct($db);
    }


    /**
     * Add a new session to the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function add(mixed $opts)
    {
        try {
            $sql = 'INSERT INTO "Sessions" ("hostID", "noOfConnectedUsers", "timeLimit") VALUES (?, ?, ?);';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $result = $sth->execute(array_map(fn($x) => $opts[$x], ["hostID", "noOfConnectedUsers", "timeLimit"]));
            return $this->db->db->lastInsertId();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function addPlayer($opts)
    {
        try {
            $sql = 'INSERT INTO "SessionPlayers" ("sessionID", "playerID") VALUES (?, ?);';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $result = $sth->execute(array_map(fn($x) => $opts[$x], ["sessionID", "playerID"]));
        // return $this->db->db->lastInsertId();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function removePlayer($opts)
    {
        try {
            $sql = 'DELETE FROM "SessionPlayers" WHERE "sessionID" = :session AND "playerID" = :player;';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            return $sth->execute($opts);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function getConnectedPlayers(mixed $opts): array
    {
        try {
            $sql = 'SELECT * FROM "SessionPlayers" WHERE "sessionID" = :id;';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            $sth->execute($opts);
            $result = array_map(fn($x) => $x['playerID'], $sth->fetchAll(PDO::FETCH_ASSOC));
            return $result;
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * Update a session in the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function update($opts)
    {
        $extraparams = "";
        if ($opts['isAnswering'] == "NULL") {
            unset($opts['isAnswering']);
            $extraparams = ", \"isAnswering\" = NULL";
        }

        $sql = $this->generateUpdateSQL($opts, $extraparams);
        $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
        $sth = bindParams($sth, $opts);
        return $sth->execute($opts);
    }

    public function addAnsweredQuestion($opts)
    {
        try {
            $sql = 'INSERT INTO "AnsweredQuestions" ("sessionID", "questionID") VALUES (:sessionID, :questionID);';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            return $sth->execute($opts);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
