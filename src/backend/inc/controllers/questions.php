<?php

/**
 * Defines Questions Controller
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>
 */


/**
 * QuestionsController Class
 * is used to manage the Questions Table
 */
class QuestionsController extends TableController
{
    public $db;
    public $table = 'Questions';

    /**
     * Initializes the TeamsController
     *
     * @param DatabaseConnection $db Instance of DatabaseConnection.
     *
     */
    public function __construct(DatabaseConnection $db)
    {

        parent::__construct($db);
    }


    /**
     * Add a new question to the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function add(mixed $opts)
    {
        try {
            $sql = 'INSERT INTO "Questions" ("questionText", "imagePath", "questionType") VALUES (?, ?, ?);';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $result = $sth->execute(array_map(fn ($x) => $opts[$x], ["questionText", "imagePath", "questionType"]));
            return $this->db->db->lastInsertId();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * Update a question in the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function update($opts)
    {
        try {
            $sql = $this->generateUpdateSQL($opts);
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            return $sth->execute($opts);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
