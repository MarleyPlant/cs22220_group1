<?php

/**
 * Defines TableController Base Class
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>

 */


/**
 * TableController Base Class
 * is used to manage a Database Table
 * and is used as a base class for all TableControllers
 * it should implement any common functionality
 */
class TableController
{
    public $db;
    public $table;

    /**
     * TableController Constructor
     *
     * @param DatabaseConnection $db Instance of DatabaseConnection.
     *
     */
    public function __construct(DatabaseConnection $db)
    {

        $this->db = $db;
    }

    /**
     * Delete a session from the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return bool
     *
     */
    public function delete(mixed $opts)
    {
        try {
            $sql = 'DELETE FROM "' . $this->table . '" WHERE "id" = :id;';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            return $sth->execute($opts);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }


    /**
     * Get All Records From Specified Table
     *
     * @return [type]
     *
     */
    public function getAll()
    {
        try {
            $sql = 'SELECT * FROM "' . $this->table . '";';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth->execute();

            $result = $sth->fetchAll();
            return $result;
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function getSingle(mixed $opts)
    {
        try {
            $sql = 'SELECT * FROM "' . $this->table . '" WHERE "id" = :id;';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            $sth->execute($opts);

            $result = $sth->fetch(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function generateUpdateSQL($opts, $extraparams = "", $pk = ['id'])
    {
        try {
            $where = array_map(fn($key) => '"' . $key . '"' . ' = :' . $key, $pk);
            $where = implode(" AND ", $where);
            $pkvalues = array_map(fn($key) => $opts[$key], $pk);
            foreach ($pk as $key) {
                unset($opts[$key]);
            }
            $params = array_map(fn($key) => '"' . $key . '"' . ' = :' . $key, array_keys($opts));
            $params = implode(", ", $params);
            $params .= $extraparams;
            $opts = array_merge($opts, $pkvalues);
            $sql = 'UPDATE ' . '"' . $this->table . '"' . ' SET ' . $params . ' WHERE ' . $where;
            return $sql;
        } catch (Exception $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
