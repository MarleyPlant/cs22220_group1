<?php

/**
 * Defines Players Controller
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>
 */


/**
 * PlayersController Class
 * is used to manage the Players Table
 */
class PlayersController extends TableController
{
    public $db;
    public $table = 'Players';

    /**
     * Initializes the PlayersController
     *
     * @param DatabaseConnection $db Instance of DatabaseConnection.
     *
     */
    public function __construct(DatabaseConnection $db)
    {

        parent::__construct($db);
    }


    /**
     * Add a new player to the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function add(mixed $opts)
    {
        try {
            $sql = 'INSERT INTO "Players" ("username") VALUES (?);';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $result = $sth->execute(array_map(fn ($x) => $opts[$x], ["username"]));
            return $this->db->db->lastInsertId();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * Update a player in the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function update($opts)
    {
        try {
            $sql = $this->generateUpdateSQL($opts);
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            return $sth->execute($opts);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
