<?php

/**
 * Defines Answers Controller
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>
 */


/**
 * AnswersController Class
 * is used to manage the Answers Table
 */
class AnswersController extends TableController
{
    public $db;
    public $table = 'Answers';

    /**
     * Initializes the AnswersController
     *
     * @param DatabaseConnection $db Instance of DatabaseConnection.
     *
     */
    public function __construct(DatabaseConnection $db)
    {

        parent::__construct($db);
    }


    /**
     * Add a new answer to the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function add(mixed $opts)
    {
        try {
            $sql = 'INSERT INTO "Answers" ("questionID", "answerText") VALUES (?, ?);';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $result = $sth->execute(array_map(fn ($x) => $opts[$x], ["questionID", "answerText"]));
            return $this->db->db->lastInsertId();
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function getFromQuestion(mixed $opts)
    {
        try {
            $sql = 'SELECT * FROM "Answers" WHERE "questionID" = :questionID;';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            $sth->execute($opts);
            return $sth->fetch(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * Update a answer in the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function update($opts)
    {
        try {
            $sql = $this->generateUpdateSQL($opts);
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            return $sth->execute($opts);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
