<?php

/**
 * Defines Scores Controller
 *
 * @package category
 * @subpackage subcategory
 * @author Marley Plant <marley@marleyplant.com>
 */


/**
 * ScoresController Class
 * is used to manage the Scores Table
 */
class ScoresController extends TableController
{
    public $db;
    public $table = 'Scores';

    /**
     * Initializes the ScoresController
     *
     * @param DatabaseConnection $db Instance of DatabaseConnection.
     *
     */
    public function __construct(DatabaseConnection $db)
    {

        parent::__construct($db);
    }

    /**
     * Add a new score to the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return array|bool
     *
     */
    public function add(mixed $opts)
    {
        try {
            $sql = 'INSERT INTO "Scores" ("playerID", "sessionID", "score") VALUES (?, ?, ?);';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $result = $sth->execute(array_map(fn($x) => $opts[$x], ["playerID", "sessionID", "score"]));
            return $result;
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    /**
     * Delete a session from the database
     *
     * @param mixed $opts Query Options Array.
     *
     * @return bool
     *
     */
    public function delete(mixed $opts)
    {
        try {
            $sql = 'DELETE FROM "' . $this->table . '" WHERE "playerID" = :playerID AND "sessionID" = :sessionID;';
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            return $sth->execute($opts);
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }

    public function update(mixed $opts)
    {
        try {
            $sql = $this->generateUpdateSQL($opts, "", ['playerID', 'sessionID']);
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            $result = $sth->execute($opts);

            return $result;
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }


    public function getScores(mixed $opts)
    {
        try {
            $params = array_map(fn($key) => '"' . $key . '"' . ' = :' . $key, array_keys($opts));
            $params = implode(" AND ", $params);
            $sql = 'SELECT * FROM "Scores" WHERE ' . $params;
            $sth = $this->db->db->prepare($sql, [PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY]);
            $sth = bindParams($sth, $opts);
            $sth->execute($opts);

            $result = $sth->fetchAll(PDO::FETCH_ASSOC);
            return $result;
        } catch (PDOException $e) {
            return ['error' => $e->getMessage()];
        }
    }
}
