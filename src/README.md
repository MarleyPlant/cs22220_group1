# **House Of Games** - React Frontend
[![VS Code Container](https://img.shields.io/badge/VS%20Code%20Container-fafa?style=for-the-badge&logo=visualstudiocode&color=007ACC&logoColor=007ACC&labelColor=2C2C32)](https://open.vscode.dev/microsoft/vscode)
[![Static Badge](https://img.shields.io/badge/Microsoft%20Teams-fwa?style=for-the-badge&logo=microsoftteams&logoColor=white&color=%236264A7)]()
[![Static Badge](https://img.shields.io/badge/DISCORD%20SERVER-faw?style=for-the-badge&logo=discord&logoColor=white&color=%237289da)]()

<br />

### 📁 **Repository Structure**

| Item            | Location        | Purpose                            |
| --------------- | --------------- | ---------------------------------- |
| UI Components   | /src/frontend   | Frontend Javascript Source         |


<br />


### 📐 **Code Style**
Javascript and Typescript code should follow the [XXXX]() Standard. In order to enforce this we have setup [ESLint](),
on top of these standard we have added the following additional rules

| Rule | Description |
|------|-------------|
|      |             |



<br />

## Installation

 ```bash
npm run build
```

then after placing the contents of `./src/frontend/build` in `/var/www/html` 
or in the university web space root of `m:/html` you can then load the compiled react frontend
In order to setup the application for development you must place the contents of `./src/` in a directory within `/var/www/html` or the university web space root of `m:/public_html`.
Ensure the parent directory has the correct permissions set to allo PHP scripts and HTML fies to run.



## Development
You can run a docker container which will setup the dependencies and run development tool

```bash
docker-compose up
```
<br />


### Manually

```bash
npm install
npm start
```

<br />

## Testing
In order to run tests on the code you can run `npm run test`
you can also use `npm run fix` to fix any error or warnings which can automatically be fixed
One person will perfrom manual tests by uploading the scripts to the university php server and writing down the results in a test table

<br />

## 💻 **TECHNOLOGIES**
![javascript](https://img.shields.io/badge/javascript-3670A0?style=for-the-badge&logo=javascript&logoColor=ffdd54)
![bootstrap](https://img.shields.io/badge/bootstrap-3670A0?style=for-the-badge&logo=bootstrap&logoColor=ffdd54)
<!-- ![sass](https://img.shields.io/badge/sass-3670A0?style=for-the-badge&logo=sass&logoColor=ffdd54) -->
<!-- ![react](https://img.shields.io/badge/react-3670A0?style=for-the-badge&logo=react&logoColor=61DAFB) -->
![fontawesome](https://img.shields.io/badge/fontawesome-3670A0?style=for-the-badge&logo=react&logoColor=#28DD7)

### 📖 **References**
- [JSDoc Tutorial](https://www.inkoop.io/blog/a-guide-to-js-docs-for-react-js/)
<!-- - [ESLint Tutorial]()
- [Material Design Bootstrap Documentation]()
- [Test Framework Documentation]() -->


<br />