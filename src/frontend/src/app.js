import React from "react";
import { Route, BrowserRouter as Router, Routes } from "react-router-dom";
import PageStart from "./pages/PageStart";
import PageAdmin from "./pages/admin";
import PageImportQuestions from "./pages/admin/import";
import PageCreateLobby from "./pages/lobby/create";
import PageJoinLobby from "./pages/lobby/join";
import PageLobby from "./pages/lobby/lobby";
import PageRestore from "./pages/lobby/restore";
import PageResults from "./pages/lobby/results";
import PageQuestion from "./pages/questions";
import PageBuzzerRound from "./pages/rounds/buzzer";
import PageBuzzerRoundEnter from "./pages/rounds/buzzer/enter";
import { ToastContainer } from "react-toastify";
import "./scss/globals.scss";

// Define the global state of the application
const defaultGlobalState = {
    isAnswering: false,
    playerid: undefined,
    sessionid: undefined,
    round: 0,
    score: 0,
};
export const globalStateContext = React.createContext(defaultGlobalState);
export const dispatchStateContext = React.createContext(undefined);

// Define the global state provider
/**
 * Define the global state provider
 * @param {object} props - The props object
 * @param {object} props.children - The child components
 * @returns {object} The global state provider
 */
const GlobalStateProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        (state, newValue) => ({ ...state, ...newValue }),
        defaultGlobalState
    );
    return (
        <globalStateContext.Provider value={state}>
            <dispatchStateContext.Provider value={dispatch}>
                {children}
            </dispatchStateContext.Provider>
        </globalStateContext.Provider>
    );
};

// Define a hook to access the global state
/**
 * Define a hook to access the global state
 * @returns {any} The global state and dispatch function hooks
 */
export const useGlobalState = () => [
    React.useContext(globalStateContext),
    React.useContext(dispatchStateContext)
];


/**
 * The main application component.
 * @returns {JSX.Element}The main application component.
 */
export function App() {

    // Define the routes of the application.
    // Each route is associated with a page component
    // The component is rendered when the route is matched in the url

    return (
        <GlobalStateProvider>
            <Router>
                <div className="App h-100 w-100">
                    <ToastContainer style={{ width: "40%" }} />
                    <Routes>
                        <Route path="/" Component={PageStart} />

                        <Route path="/lobby/create" Component={PageCreateLobby} />
                        <Route path="/lobby/:id" Component={PageLobby} />
                        <Route path="/lobby/:id/results" Component={PageResults} />
                        <Route path="/lobby/join" Component={PageJoinLobby} />
                        <Route path="/lobby/join/:id" Component={PageJoinLobby} />
                        <Route path="/lobby/restore" Component={PageRestore} />

                        <Route path="/lobby/:id/rounds/buzzer/:qid" Component={PageBuzzerRound} />
                        <Route path="/lobby/:id/rounds/buzzer/:qid/enter" Component={PageBuzzerRoundEnter} />

                        <Route path="/admin" Component={PageAdmin} />
                        <Route path="/admin/import" Component={PageImportQuestions} />

                        <Route path="/question/:id" Component={PageQuestion} />
                        <Route path="/question/" Component={PageQuestion} />
                    </Routes>
                </div>
            </Router>
        </GlobalStateProvider>
    );
}
