import React, { useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { getAnswer, getQuestion, getSession, incrementScore } from "../../../api";
import { useGlobalState } from "../../../app";
import { QuestionCard, AnswerForm } from "../../../components";
import { GamePageLayout } from "../../../layouts/GamePageLayout";
import { setupRoundInterval } from "../../../util/rounds/checks";
import { nextQuestion, stopAnswering } from "../../../util/rounds/navigation";
import { ALERTUSER } from "../../../util/global";

/**
 * Enter an answer and check if it is correct or incorrect.
 * @returns {JSX.Element} The Buzzer Round Enter Page
 */
export default function PageBuzzerRoundEnter() {
    let { id, qid } = useParams();
    let [question, setQuestion] = React.useState({});
    let [answer, setAnswer] = React.useState("");
    let [session, setSession] = React.useState({});
    let [state, dispatch] = useGlobalState();
    let [answeredIncorrectly, setAnsweredIncorrectly] = React.useState(false);


    const navigate = useNavigate();

    useEffect(() => {
        getQuestion(qid).then(data => {
            setQuestion(data);
        });

        getAnswer(qid).then(data => {
            setAnswer(data);
        });

        getSession(id).then(session => {
            setSession(session);
        });


        let interval = setupRoundInterval(id, qid, dispatch, state, navigate);
        return () => clearInterval(interval);
    }, [qid, id, dispatch, state, navigate]);

    return <GamePageLayout session={session} state={state} dispatch={dispatch} roundType="Buzzer">
        <div className="w-50">
            <QuestionCard title={question.questionText} id={question.id}></QuestionCard>
            {answeredIncorrectly ? (<p>You can no longer buzz in on this question.</p>) : (
                <AnswerForm onSubmit={(val) => {
                    if (val.toLowerCase() == answer.answerText.toLowerCase()) {
                        // Set Correct
                        ALERTUSER("Correct!", "SUCCESS", null, () => {
                            incrementScore(id, state, dispatch).then(() => {
                                stopAnswering(id).then(() => {
                                    nextQuestion(id, state, dispatch).then((url) => {
                                        navigate(url);
                                    });
                                });
    
                            });
                        });
                    } else {
                        ALERTUSER("Incorrect!", "ERROR", null, () => {
                            stopAnswering(id);
                            setAnsweredIncorrectly(true);
                        });
                    }
                }}></AnswerForm>
            )}
        </div>

    </GamePageLayout>;
}
