import React, { useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { getSession, getQuestion } from "../../../api";
import { useGlobalState } from "../../../app";
import { startAnswering } from "../../../util/rounds/navigation";
import { setupRoundInterval } from "../../../util/rounds/checks";
import { GamePageLayout } from "../../../layouts/GamePageLayout";
import { QuestionCard } from "../../../components/QuestionCard";
import { ALERTUSER } from "../../../util/global";


/**
 * Buzzer Round Main Page.
 * @returns {JSX.Element} The Buzzer Round Page
 */
export default function PageBuzzerRound() {
    let { qid, id } = useParams();
    let [question, setQuestion] = React.useState({});
    let [session, setSession] = React.useState({});
    const [state, dispatch] = useGlobalState();
    const navigate = useNavigate();

    useEffect(() => {
        // Ran On Every Render Of the Page

        // Get The Question From Database and Set Local State
        getQuestion(qid).then(data => {
            setQuestion(data);
        });

        getSession(id).then(session => {
            setSession(session);
        });

        // Setup The Interval For The Round
        let interval = setupRoundInterval(id, qid, dispatch, state, navigate);
        return () => clearInterval(interval);
    }, [qid, id]);

    return <GamePageLayout session={session} state={state} dispatch={dispatch} roundType="Buzzer">
        <div className="w-50">
            <QuestionCard title={question.questionText} id={question.id}></QuestionCard>
            <button
                data-mdb-ripple-init
                type="submit"
                disabled={state.isAnswering}
                className="btn btn-success btn-block btn-lg"
                onClick={() => {
                    // Disable The Button For Other Users



                    startAnswering(id, state).then((data) => {
                        if (data == true) {
                            navigate(`/lobby/${id}/rounds/buzzer/${qid}/enter`);
                        } else {
                            ALERTUSER("You can't buzz in right now", "ERROR");
                        }
                    });
                }}
            >
                <i className="fa fa-bell me-2" />
            </button>
        </div>

    </GamePageLayout>;
}
