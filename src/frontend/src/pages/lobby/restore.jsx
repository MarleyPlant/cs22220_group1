import React from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault";
import { useState, useEffect } from "react";
import { useGlobalState } from "../../app.js";
import { useNavigate } from "react-router-dom";
import { deleteSession, getSessions, disconnectPlayer, getPlayer } from "../../api";
import { DeleteButton, JoinGameButton, PlayerChip } from "../../components";
import { ALERTUSER } from "../../util/global.js";

/**
 * Component for restoring a game.
 * @returns {JSX.Element} - The Restore Game Page
 */
export default function PageRestore() {
    const [sessions, setSessions] = useState([]);
    const [state] = useGlobalState();
    const navigate = useNavigate();

    /**
     * Function to update sessions.
     */
    async function updateSessions() {
        let session = await getSessions();

        session = session.filter((session) => {
            return session.hostID == state.playerid;
        });

        session = session.map(async (session) => {
            let players = await session.players.map(async (player) => {
                return await getPlayer(player, session.id);
            });
            session.players = await Promise.all(players);
            return await session;
        });

        session = await Promise.all(session);
        setSessions(await session);
    }

    useEffect(() => {
        // Ran On Every Render Of the Page
        updateSessions();

        const interval = setInterval(() => {
            // Update Sessions Every 10 Seconds
            updateSessions()
        }, 10000)
        return () => clearInterval(interval)
    });

    /**
     * Generate A Session Card.
     * @param {object} session - Session data.
     * @returns {JSX.Element}- Session card component.
     */
    function generateSessionCard(session) {
        return <div key={session.id} className="card">
            <h5 className="card-header">Gamecode: {session.id}</h5>
            <div className="card-body">
                {session.players.map((player) => {
                    return <PlayerChip key={player.id} player={player} showRemove={true} remove={async (event) => {
                        event.preventDefault();
                        disconnectPlayer(player.id, session.id).then(() => {
                            updateSessions();
                        });
                    }}></PlayerChip>
                })}
            </div>
            <div className="d-flex card-footer gap-2">
                <JoinGameButton style="success" onClick={() => {
                    navigate("/lobby/" + session.id);
                }}/>
                <DeleteButton onClick={async (event) => {
                    event.preventDefault();
                    ALERTUSER("Game Deleted", "SUCCESS", null, async () => {
                        await deleteSession(session.id);
                        await updateSessions();
                    })

                }} />
            </div>
        </div>
    }

    return (
        <HOGDefaultLayout showScore={false} showRound={false}>
            <div className="d-flex flex-col gap-5">
                {sessions.length === 0 && <h2>No Games Found</h2>}

                {sessions.map((session) => {
                    return generateSessionCard(session);
                })
                }
            </div>

        </HOGDefaultLayout>
    );
}
