import React, { useState, useEffect } from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault";
import { useParams } from "react-router-dom";
import { getPlayer, getSession, getAllscores } from "../../api";
import { HOGSmallLogo,PlayerChip } from "../../components";

/**
 * Component for displaying results
 * @returns {JSX.Element} - Displaying results
 */
export default function PageResults() {
    const [players, setPlayers] = useState([]);
    // eslint-disable-next-line no-unused-vars
    const [session, setSession] = useState({});
    let { id } = useParams();

    /**
     * Function to refresh game data.
     */
    function refreshData() {
        let sessiondata = getSession(id);
        let allScores = getAllscores(id);
        
        allScores.then(scores => {
            sessiondata.then(data => {
                setSession(data);

                let newdata = data['players'].map(async (player) => {
                    return getPlayer(player).then(playerdata => {
                        playerdata.isHost = player === data.hostID;
                        playerdata.score = scores.find(score => {
                            return score.playerID === playerdata.id;
                        });
                        if (playerdata.score === undefined) {
                            playerdata.score = 0;
                        } else {
                            playerdata.score = playerdata.score.score;
                        }

                        return playerdata;
                    });
                });
                Promise.all(newdata).then(data => {
                    const sortedPlayers = data.sort((a, b) => b.score - a.score); // kap62 - Added this to sort the players into order, let me know if it works
                    setPlayers(sortedPlayers);
                });
            });
        });
    }

    useEffect(() => {
        refreshData();
    }, []);

    return (
        <HOGDefaultLayout showScore={false} showRound={false}>
            <HOGSmallLogo height={100} />
            <div className="page-title">
                <h2 className="text-center mx-auto">Results For Game {id}</h2>
            </div>

            <div className="scores my-4 d-flex gap-2 flex-column">
                {players.map((player, index) => (
                    <div className="score d-flex flex-row" key={index}>
                        <div className="chip">{player.score}</div>
                        <PlayerChip className="w-100" key={index} player={player} />
                    </div>
                ))}
            </div>
        </HOGDefaultLayout>
    );
}