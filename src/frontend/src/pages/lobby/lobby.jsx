import React from "react";
import { useState } from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault";
import { useParams } from "react-router-dom";
import { useEffect } from "react";
import { PlayerChip } from "../../components/PlayerChip";
import { useGlobalState } from "../../app";
import { useNavigate } from "react-router-dom";
import { disconnectPlayer, getPlayer, getQuestion, getSession } from "../../api";
import { nextQuestion } from "../../util/rounds/navigation";
import { removeIfNotInGame, checkIfGameEnded, checkIfGameExists } from "../../util/rounds/checks";
import { getTypeURL } from "../../util/rounds/navigation";


/**
 * Main Lobby Page.
 * @returns {JSX.Element} The Lobby Page
 */
export default function PageLobby() {
    const [players, setPlayers] = useState([]);
    const [session, setSession] = useState({});
    const [state, dispatch] = useGlobalState();
    const navigate = useNavigate();
    let { id } = useParams();

    /**
     * Function to refresh the lobby.
     */
    async function refreshData() {
        let sessiondata = getSession(id);
        sessiondata.then(data => {

            // If Session Does Not Exist, Navigate User To Home
            checkIfGameExists(data, navigate);
            checkIfGameEnded(data, id, navigate);

            // Set Session In Local State To Database Session
            setSession(data);

            // Remove Player if Not In Game
            removeIfNotInGame(data, state, navigate);

            // If Session Started, Navigate User To Relevant Round
            if (data['currentRound'] > 0) {

                // Find The Question ID, and Type Then Navigate To The Round
                let questionID = data['currentQuestion'];
                getQuestion(questionID).then((question) => {
                    let roundtype = getTypeURL(question.questionType);
                    navigate(`/lobby/${id}/rounds/${roundtype}/${questionID}`);
                });
            }

            // Get Connected Players
            let newdata = data['players'].map(async (player) => {
                // For Each Player Get Their Data
                return getPlayer(player).then(playerdata => {
                    playerdata.isHost = player === data.hostID;
                    return playerdata;
                });
            });

            // Resolve all promises and set the players into state
            Promise.all(newdata).then(data => {
                setPlayers(data);
            });
        });
    }

    useEffect(() => {
        // Ran On Every Render Of the Page

        refreshData();

        const interval = setInterval(() => {
            // this will refresh component every 0.5 seconds
            refreshData()
        }, 1000)
        return () => clearInterval(interval)
    }, []);
    return (
        <HOGDefaultLayout showScore={false} showRound={false}>

            <div className="page-title">
                <h2 className="text-center mx-auto">Waiting for Users</h2>
                <br />
                <h5 id="gamecode" className="text-center mx-auto">
                    Game Code: {id}
                </h5>
                <br />
                <br />
            </div>
            <div className="text-center mx-auto">
                <div className="d-flex" id="connectedPlayers">
                    {
                        players.map((player) => {
                            return (
                                <PlayerChip key={player.id} showRemove={session.hostID == state.playerid} player={player} remove={() => {
                                    disconnectPlayer(player.id, id).then(() => {
                                        refreshData();
                                    });
                                }}></PlayerChip>
                            )
                        })
                    }
                </div>
            </div>
            <br />
            <br />
            <div className="mx-auto my-5 text-center justify-content-center align-items-center w-100">
                <button type="button" className="btn btn-success" disabled={session.hostID !== state.playerid || players.length < 2} onClick={() => {
                    state.round = 0;
                    dispatch(state);
                    nextQuestion(id, state, dispatch).then(data => {
                        navigate(data);
                    });
                }}>
                    Start Game
                </button>
            </div>

        </HOGDefaultLayout>
    );
}
