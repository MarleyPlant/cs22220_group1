import React from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault";
import { isfull } from "../../util/rounds/checks.js";
import { addPlayer, connectPlayer, getSessions } from "../../api";
import { useParams } from "react-router-dom";
import { useGlobalState } from "../../app.js";
import { useNavigate } from "react-router-dom";

import { InputField, JoinGameButton, UsernameField } from "../../components";

import { ALERTUSER } from "../../util/global.js";

/**
 * Join Lobby Page.
 * @returns {JSX.Element} - Join Lobby page
 */
export default function PageJoinLobby() {
    let { id } = useParams();
    const [state, dispatch] = useGlobalState();
    const navigate = useNavigate();

    /**
     * Join a lobby.
     * @param {Event} event - The event object.
     */
    async function joinLobby(event) {
        event.preventDefault();


        // Get Username from Input If Not Set In Global State
        let username;
        if(state.playerid == undefined){
            username = document.getElementById("name").value || "Player";
        } else {
            username = state.playerid;
        }


        // Get Game Code From Input
        let gameID = document.getElementById("gamecode").value;

        // Get All Game Codes
        let gamecodes = await getSessions();
        gamecodes = gamecodes.map((game) => game.id);
        // Check If Game Code Exists
        if (gamecodes.includes(parseInt(gameID))) {
            let sessionHasSpace = await isfull(gameID);
            if (sessionHasSpace) {
                ALERTUSER("Game is full", "WARNING");
                return;
            }


            // Add Player To Database
            let playerID = await addPlayer(username, state, dispatch);
            await connectPlayer(playerID, gameID);

            // Update Global State
            await dispatch({ playerid: playerID, sessionid: gameID });

            // Navigate To Lobby
            navigate("/lobby/" + gameID);


        } else {
            ALERTUSER("Game code not found", "WARNING");
        }
    }

    return (
        <HOGDefaultLayout showScore={false} showRound={false}>
            <form id="joingame">
                <h3 className="text-center">
                    <b>Enter your name and game code to continue:</b>
                </h3>
                <br />
                <br />
                <div className="mb-3 w-100 mx-auto">
                    { !state.playerid ? <UsernameField></UsernameField> : <></>}

                    <br />
                    <label htmlFor="#" className="form-label">
                        Game Code:
                    </label>
                    <InputField type="text" className="form-control" id="gamecode" defaultValue={id} required="" />
                    <br />
                    <JoinGameButton className="w-100" style="success" onClick={joinLobby}></JoinGameButton>
                </div>
            </form>

        </HOGDefaultLayout>
    );
}
