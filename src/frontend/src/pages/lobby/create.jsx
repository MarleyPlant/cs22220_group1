import React from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault";
import { createNewSession, addPlayer, getPlayer } from "../../api";
import { useNavigate } from "react-router-dom";
import { useGlobalState } from "../../app.js";
import { UsernameField } from "../../components/forms/fields/UsernameField.jsx";
import { ALERTUSER } from "../../util/global.js";

/**
 * Create Lobby Page.
 * @returns {JSX.Element} The Create Lobby Page.
 */
export default function PageCreateLobby() {
    const navigate = useNavigate();
    const [state, dispatch] = useGlobalState();

    /**
     * Create A New Lobby.
     * @param {Event} event - The event object.
     */
    async function createGame(event) {
        event.preventDefault();

        console.log("Creating Game");
        
        // If Player ID Not Set In Global State, Get It From Input
        let hostname = "";
        
        if (state.playerid === undefined) {
            hostname = document.getElementById("name").value;
        } else {
            hostname = await getPlayer(state.playerid).name;
        }
        
        if (hostname === "") {
            ALERTUSER("Please enter a name", "WARNING");
            return;
        }

        // Add Player To Database
        let playerID = await addPlayer(hostname, state, dispatch);

        // Create New Session And Navigate To Lobby
        createNewSession(parseInt(playerID), navigate);
    }

    return (
        <HOGDefaultLayout showScore={false} showRound={false}>
            <form className="h-100" id="create_game_form">
                <div className="mb-3 h-100 w-100 mx-auto">
                    <h3>
                        <b>Host Your Own Game:</b>
                    </h3>
                    <br />
                    <br />
                    { state.playerid == undefined ? <UsernameField></UsernameField> : <></>}
                    <br />
                    <button
                        onClick={createGame}
                        className="btn btn-success w-100"
                        role="button"
                    >
                        <i className="fa-solid mx-2 fa-gamepad" />
                        Create Game
                    </button>
                </div>
            </form>
        </HOGDefaultLayout>
    );
}
