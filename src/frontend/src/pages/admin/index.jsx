import React from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault";
import { useState } from "react";
import { useEffect } from "react";
import { getQuestions } from "../../api/questions.js";
import { Link } from "react-router-dom";
import { EditQuestionCard } from "../../components/EditQuestionCard.jsx";

/**
 * Component for displaying the admin panel.
 * @returns {JSX.Element} - Admin Panel Component
 */
export default function PageAdmin() {
    const [questions, setQuestions] = useState([]);

    /**
     * Function to update the questions.
     */
    function updateQuestions() {
        getQuestions().then(data => {
            setQuestions(data);
        });
    }

    useEffect(() => {
        updateQuestions();
    }, []);

    return (
        <HOGDefaultLayout showScore={false} showRound={false}>
            <div className="m-5 w-50">
                <h2 className="text-center mx-auto">Admin Panel</h2>
                <br />
                <div className="text-center w-100 px-5">
                    <div className="btn-group w-100">
                        <Link className="btn btn-success" to="/question/">
                            <i className="fa-solid fa-plus me-2" />
                            New Question
                        </Link>
                        <Link className="btn btn-warning" to="/admin/import">
                            <i className="fa-solid fa-upload me-2" />
                            Import Questions
                        </Link>
                    </div>
                </div>
                <div id="questionlist" className="my-5 w-100 px-5">
                    {questions.map((question) => {
                        return (
                            <EditQuestionCard key={question.id} update={updateQuestions} id={question.id} title={question.questionText}></EditQuestionCard>
                        )
                    })}
                </div>
               
            </div>

        </HOGDefaultLayout>
    );
}
