import React from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault";
import { createQuestion } from "../../api/questions.js";
import { createAnswer } from "../../api/answers.js";
import { ALERTUSER } from "../../util/global.js";
import { useNavigate } from "react-router-dom";

/**
 * Component for importing questions from a JSON file.
 * @returns {object} The component.
 */
export default function PageImportQuestions() {
    const navigate = useNavigate();

    /**
     * Component to import questions from JSON.
     * @param {Event} event - The event object.
     */
    function importQuestions(event) {
        event.preventDefault();
        let file = document.getElementById("uploadImage").files[0];
        if (!file) {
            ALERTUSER("Please select a file", "ERROR");
            return;
        }
        let reader = new FileReader();
        /**
         *
         * @param {Event} e - Event object
         */
        reader.onload = function (e) {
            let data;
            try {
                data = JSON.parse(e.target.result);
                data.forEach(async (element) => {
                    let questionid = await createQuestion(element.question, element.type, element.image);
                    await createAnswer(parseInt(questionid.success), element.answer);
                });
            } catch (e) {
                ALERTUSER("Invalid JSON file", "ERROR");
            } finally {
                ALERTUSER(`${data.length} Questions imported successfully`, "SUCCESS", null, () => {
                    navigate("/admin/");
                });
            }
            
        }
        reader.readAsText(file);
    }

    return (
        <HOGDefaultLayout showScore={false} showRound={false}>
            <form id="createquestion" className="w-100">
                <fieldset>
                    <h4 className="text-center mx-auto">Admin Panel</h4>
                    <legend className="text-center mx-auto">Import Questions From JSON</legend>
                    <br />
                    <div className="mb-3 w-50 mx-auto">
                        <label htmlFor="uploadImage" className="form-label">
                            Upload JSON:
                        </label>
                        <input className="form-control" type="file" id="uploadImage"/>
                    </div>
                    <div className="mb-3 w-50 mx-auto">
                        <button onClick={importQuestions} className="btn btn-success text-center w-100">
                            Submit
                        </button>
                    </div>
                </fieldset>
            </form>


        </HOGDefaultLayout>
    );
}
