import React from "react";
import { HOGDefaultLayout } from "../../layouts/LayoutDefault.jsx";
import { createQuestion, getQuestion, updateQuestion, createAnswer, getAnswer, updateAnswer } from "../../api/index.js";
import { useParams } from "react-router-dom";
import { InputField } from "../../components/forms/InputField.jsx";
import { MDBBtn } from 'mdb-react-ui-kit';
import { ALERTUSER } from "../../util/global.js";
import { useNavigate } from "react-router-dom";

/**
 * Component for creating or editing a question.
 * @returns {JSX.Element} The component.
 */
export default function PageQuestion() {
    let { id } = useParams();
    const navigate = useNavigate();
    let action = "Create";
    if (id) {
        action = "Edit";
        getQuestion(id).then(async data => {
            let answer = await getAnswer(data.id);
            document.getElementById("inputQuestion").value = data.questionText;
            document.getElementById("inputAnswer").value = answer.answerText;
            document.getElementById("disabledSelect").value = data.questionType;
        });
    }



    /**
     * Add a new question
     * @param {Event} event - The event object
     */
    const addQuestion = async (event) => {
        event.preventDefault();
        let question = document.getElementById("inputQuestion").value;
        let answer = document.getElementById("inputAnswer").value;
        let type = document.getElementById("disabledSelect").value;
        let questionid = await createQuestion(question, type);
        let answerid = await createAnswer(parseInt(questionid.success), answer);
        if (questionid.success && answerid.success) {
            ALERTUSER("Question and Answer added successfully", "SUCCESS", () => {
                navigate("/admin");
            });
        }
    }

    /**
     * Update an existing question
     * @param {Event} event - The Event Object
     */
    const update = async (event) => {
        event.preventDefault();
        let question = document.getElementById("inputQuestion").value;
        let answer = document.getElementById("inputAnswer").value;
        document.getElementById("disabledSelect").value;
        let questionid = id;
        let answerid = await getAnswer(questionid);
        let isUpdatedA = await updateAnswer(parseInt(answerid.id), answer);
        let isUpdatedQ = await updateQuestion(parseInt(questionid), question);
        if (isUpdatedA && isUpdatedQ) {
            ALERTUSER("Question and Answer updated successfully", "SUCCESS", () => {
                window.location.href = "/admin";
            });
        }

    }

    return (
        <HOGDefaultLayout showScore={false} showRound={false}>
            <form id="createquestion" className="w-100">
                <fieldset>
                    <h4 className="text-center mx-auto">Admin Panel</h4>
                    <legend className="text-center mx-auto">{action} a Question</legend>
                    <br />
                    <div className="mb-3 w-50 mx-auto">
                        <label htmlFor="disabledSelect" className="form-label">
                            Question Type
                        </label>
                        <select id="disabledSelect" className="form-select" required="">
                            <option>Buzzer Round</option>
                            <option>Pairs Round</option>
                            <option>Answer Smash Round</option>
                            <option>Tablet Round</option>
                        </select>
                    </div>
                    <div className="mb-3 w-50 mx-auto">
                        <label htmlFor="inputQuestion" className="form-label">
                            Question:
                        </label>
                        <InputField
                            type="text"
                            id="inputQuestion"
                            className="form-control"
                            placeholder="Question"
                            required=""
                        />
                    </div>
                    <div className="mb-3 w-50 mx-auto">
                        <label htmlFor="inputAnswer" className="form-label">
                            Answer:
                        </label>
                        <InputField
                            type="text"
                            id="inputAnswer"
                            className="form-control"
                            placeholder="Answer"
                            required=""
                        />
                    </div>
                    {/* <div className="mb-3 w-50 mx-auto">
                        <label htmlFor="uploadImage" className="form-label">
                            Upload Image:
                        </label>
                        <MDBFile className="form-control" type="file" id="uploadImage" onChange={updateImage} />
                    </div> */}
                    <div className="mb-3 w-50 mx-auto">
                        <MDBBtn onClick={(event) => {
                            if (id) {
                                update(event);
                            } else {
                                addQuestion(event);
                            }
                        }} className="w-100" color="success">
                            Submit
                        </MDBBtn>
                    </div>
                </fieldset>
            </form>


        </HOGDefaultLayout>
    );
}