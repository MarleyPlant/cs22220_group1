import React from "react";
import { Link } from "react-router-dom";
import { SocialIcons, HOGBigLogo, JoinGameButton } from "../components/index";
import { useNavigate } from "react-router-dom";

/**
 * The Start Page of the application.
 * Each Button links to a different page.
 * This is done with the Link component from react-router-dom.
 * @returns {JSX.Element} - Start page
 */
function PageStart() {
    const navigate = useNavigate();

    return (
        <div className="min-vh-100 d-flex flex-column align-items-center justify-content-center">
            <HOGBigLogo></HOGBigLogo>
            <div
                className="btn-group mt-5 w-50"
                role="group"
                aria-label="Basic example"
            >
                <Link
                    type="button"
                    className="btn btn-primary"
                    data-mdb-ripple-init
                    to="/lobby/create"
                >
                    <i className="fa fa-people-group me-2"></i>
                    Host A Game
                </Link>
                <JoinGameButton onClick={() => {
                    navigate("/lobby/join")
                }}></JoinGameButton>
                <Link
                    to="/lobby/restore"
                    type="Link"
                    className="btn btn-primary"
                    data-mdb-ripple-init
                >
                    <i className="fa fa-window-restore me-2"></i>
                    Restore Game
                </Link>
                <Link
                    to="/admin"
                    type="button"
                    className="btn btn-primary"
                    data-mdb-ripple-init
                >
                    <i className="fa fa-cog me-2"></i>
                    Admin Panel
                </Link>
            </div>

            <SocialIcons></SocialIcons>
        </div>
    );
}

export default PageStart;
