
/**
 * Get Image From Form Event and Converrt to Base64.
 * @param {object} e - Form Event.
 * @param {Function} setImage - Set Image Function.
 */
export async function getbaseimg(e, setImage) {
    let file = e.target.files[0];
    let base64String;
    let reader = new FileReader();

    /**
     * Callback function executed when the file is loaded
     */
    reader.onload = function () {
        base64String = reader.result.replace("data:", "")
            .replace(/^.+,/, "");


        // alert(imageBase64Stringsep);
        setImage(base64String);
    }
    await reader.readAsDataURL(file);
}