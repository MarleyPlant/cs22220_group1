import { getSession } from "../api/sessions";

/**
 * Udates IsAnswering.
 * @param {object} session - Session Data.
 * @param {object} state - Global State.
 * @param {Function} dispatch - Global State Dispatcher.
 */
export function updateIsAnswering(session, state, dispatch) {
    state.isAnswering = (session.isAnswering !== state.playerid) && session.isAnswering !== null;
    dispatch(state);
}


/**
 * Update Global State From Session ID.
 * @param {string} sessionID - Session ID.
 * @param {Function} dispatch - Global State Dispatcher.
 * @param {object} state - Global State.
 */
export async function updateGlobalStateFromSessionID(sessionID, dispatch, state) {
    let session = await getSession(sessionID);
    state.round = session.currentRound;

    dispatch(state);
}

/**
 * Update Global State From Session.
 * @param {object} session - Session Data.
 * @param {Function} dispatch - Global State Dispatcher.
 * @param {object} state - Global State.
 */
export async function updateGlobalStateFromSession(session, dispatch, state) {
    state.round = session.currentRound;

    dispatch(state);
}