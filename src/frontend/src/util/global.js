import { toast } from 'react-toastify';
import { DEBUG } from '../config';

const DEBUGHANDLERS = {
    USER: (message) => {
        console.log(message);
    },
    WARNING: (message) => {
        console.warn(`WARNING: ${message}`);
    },
    ERROR: (message, data) => {
        if (data == null) {
            return;
        }

        if (data.error) {
            console.error(`${message}: ${data.error}`);
            console.table(data);
            return;
        } else {
            console.error(`${message}`);
        }

        if (data instanceof Object) {
            console.table(data);
        }
    },
    API: (message) => {
        console.log(`Generated URL For API Call: ${message}`);
    }
}

/**
 * Log Message To Console If Debug Is Enabled.
 * @param {string} message - Message To Log.
 * @param {number} debuglevel - Debug Level.
 * Debug Levels
 * USER - Information For End User
 * WARNING - Warning Messages
 * ERROR - Error Messages
 * API - API Calls
 * @param {any} data - Data To Log.
 */
export function DEBUGLOG(message, debuglevel = "USER", data = null) {
    if (DEBUG.includes(debuglevel)) {
        debuglevel = debuglevel.toUpperCase();
        DEBUGHANDLERS[debuglevel](message, data);
    }
}

/**
 * Alert User With Message.
 * @param {string} message - Message To Display.
 * @param {string} level - Severity Level.
 * @param {Function} action - Action To Execute On Close.
 * @param {Function} onOpen - Action To Execute On Open.
 */
export async function ALERTUSER(message, level = "INFO", action = null, onOpen = null) {
    if (onOpen) {
        await onOpen();
    }
    let options = {
        onClose: action,
        autoClose: 500,
        position: "top-center",
        toastId: message,
    };

    switch (level) {
    case "INFO":
        toast.info(message, options);
        break;
    case "WARNING":
        toast.warning(message, options);
        break;
    case "ERROR":
        toast.error(message, options);
        break;
    case "SUCCESS":
        toast.success(message, options);
        break;
    default:
        toast(message, options);
        break;
    }
}