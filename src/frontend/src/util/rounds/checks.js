import { getSession } from "../../api/sessions";
import { isNextQuestion, getNextQuestionURL } from "./navigation";
import { updateIsAnswering } from "../state";
import { updateGlobalStateFromSession } from "../state";
import { ALERTUSER, DEBUGLOG } from "../global";


/**
 * Check if a game exists based on the response data
 * @param {object} data - Response Data
 * @param {Function} navigate - Moves user to a new URL
 */
export function checkIfGameExists(data, navigate) {
    if (data.error === true) {
        // navigate(`/lobby/${sessionID}/results`);
        navigate(`/`);

        return;
    }
}

/**
 * Check If Session Is Full.
 * @param {string} sessionID - Session ID.
 * @returns {boolean} If Session Is Full.
 */
export async function isfull(sessionID) {
    let session = await getSession(sessionID);
    return session.players.length >= 5;
}


/**
 * Check if the game has ended based on the response data
 * @param {object} data - Response Data
 * @param {string} sessionID - ID of the session
 * @param {Function} navigate - Moves user to a new URL
 */
export function checkIfGameEnded(data, sessionID, navigate) {
    if (data.hasEnded == true) {
        navigate(`/lobby/${sessionID}/results`);
        return;
    }
}


/**
 * Check If Player Is In Session.
 * @param {object} session - Session Data.
 * @param {object} state - Global State.
 * @returns {boolean} If Player Is In Game.
 */
export function checkIfPlayerInGame(session, state) {
    // If Player Is Not In Session, Redirect To Home
    if (session['players'].includes(state.playerid)) {
        return true;
    }

    return false;
}

/**
 * Set Up Round Interval Function
 * Runs Active Game Checks Every 500ms.
 * @param {string} id - Session ID.
 * @param {string} qid - Question ID.
 * @param {Function} dispatch - Global State Dispatcher.
 * @param {object} state - Global State.
 * @param {Function} navigate - Navigation Function.
 * @returns {number} The Interval ID.
 */
export function setupRoundInterval(id, qid, dispatch, state, navigate) {
    let interval = setInterval(() => {
        DEBUGLOG("Running Round Interval", "LIFECYCLE");
        // Runs Active Game Checks Every 500ms
        activeGameChecks(id, dispatch, state, navigate, interval);

        // Checks if this question has been asnwered
        // If it has, then navigate to the next question
        isNextQuestion(id, qid).then(isNext => {
            if (isNext) {
                qid = id;
                getNextQuestionURL(id).then(data => {
                    clearInterval(interval);
                    navigate(data);
                });

            }
        });

    }, 500);

    return interval
}

/**
 * Remove Player If Not In Game.
 * @param {object} data - Session Data.
 * @param {object} state - Global State.
 * @param {Function} navigate - Navigation Function.
 * @param {number} interval - Interval ID.
 */
export async function removeIfNotInGame(data, state, navigate, interval=null) {
    if (!checkIfPlayerInGame(data, state)) {
        if(interval) {
            await clearInterval(interval);
        }
        await navigate("/");
        ALERTUSER("You have been removed from the game", "INFO");
    }
}

/**
 * Active Game Checks Function
 * Runs Necessary Checks For Active Game.
 * @param {string} id - Session ID.
 * @param {Function} dispatch - Global State Dispatcher.
 * @param {object} state - Global State.
 * @param {Function} navigate - Navigation Function.
 * @param {number} interval - Interval ID.
 */
export function activeGameChecks(id, dispatch, state, navigate, interval) {
    getSession(id).then(session => {
        checkIfGameExists(session, navigate);
        checkIfGameEnded(session, id, navigate);
        removeIfNotInGame(session, state, navigate, interval);

        // Update Global State From Session
        updateGlobalStateFromSession(session, dispatch, state);

        // Periodically Check If A User Is Answering The Question
        updateIsAnswering(session, state, dispatch);
    });
}
