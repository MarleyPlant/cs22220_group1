import { getSession, updateSession } from "../../api/sessions";
import { getQuestions, getQuestion } from "../../api/questions";
import { ALERTUSER } from "../global";

/**
 * Get URL For Next Question.
 * And Update Session Data.
 * @param {string} sessionID - Session ID.
 * @param {object} state - Global State.
 * @param {Function} dispatch - Global State Dispatcher.
 * @returns {string} URL For Next Question.
 */
export async function nextQuestion(sessionID, state, dispatch) {
    // Get Current Session Data
    let currentSession = await getSession(sessionID);

    // Checks if it is the last round and if so send to results page
    if (state.round >= 5) {
        currentSession.hasEnded = true;
        await updateSession(currentSession);
        return `/lobby/${sessionID}/results`;
    }

    // Select a random question
    let questions = await getQuestions();

    if(questions.length == 0){
        ALERTUSER("No Questions Found", "ERROR", () => {
            return `/admin`; 
        });
    }

    let question = await questions[Math.floor(Math.random() * questions.length)];


    let type = getTypeURL(question.questionType);
    state.round += 1;
    dispatch(state);

    // Update Session In database to include round data

    // Modify To Next Question And Round
    if (!currentSession.currentRound) {
        currentSession.currentRound = 1;
    } else {
        currentSession.currentRound += 1;
    }
    currentSession.currentQuestion = question.id;

    // Update Session Data On Database#
    await updateSession(currentSession);

    return `/lobby/${sessionID}/rounds/${type}/${question.id}`;

}

/**
 * Start Answering The Question.
 * @param {number} id - The Session ID.
 * @param {object} state - The Global State.
 * @returns {boolean} If The Player Can Answer and Answering state has been saved to database
 */
export async function startAnswering(id, state) {
    let session = await getSession(id);
    if (session.isAnswering != "NULL") {
        session.isAnswering = state.playerid;
        updateSession(session);
        return true;
    }
    return false;
}

/**
 * Stop Answering The Question.
 * @param {number} id - The Session ID.
 */
export async function stopAnswering(id) {
    let session = await getSession(id);
    session.isAnswering = "NULL";
    await updateSession(session);
}


/**
 * Check if The round has moved on to the next question.
 * @param {string} sessionID - The Session ID.
 * @param {string} currentQuestion - The Current Question ID.
 * @returns {Promise} If The Round Has Moved On.
 */
export async function isNextQuestion(sessionID, currentQuestion) {
    console.log("Checking If Next Question");
    let session = await getSession(sessionID);
    currentQuestion = parseInt(currentQuestion);

    if (session.currentQuestion == currentQuestion) {
        return false;
    }

    return true;
}

/**
 * Get Next Question URL.
 * @param {string} sessionID - The Session ID.
 * @returns {Promise} The URL For The Next Question.
 */
export async function getNextQuestionURL(sessionID) {
    console.log("Getting Next Question URL");
    let session = await getSession(sessionID);
    let question = await getQuestion(session.currentQuestion);
    let type = getTypeURL(question.questionType);


    return `/lobby/${sessionID}/rounds/${type}/${question.id}`;
}


/**
 * Get Type URL For Round Type.
 * @param {string} type - The Round Type.
 * @returns {string} The Round Type As Used In THE URL.
 */
export function getTypeURL(type) {
    switch (type) {
    case "Buzzer Round":
        return "buzzer";
    case "Pairs Round":
        return "pairs";
    case "Tablet Round":
        return "tablet";
    case "Answer Smash Round":
        return "smash";
    }
}