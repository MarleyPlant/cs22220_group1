import React from "react";

/**
 * A component for displaying a question card
 * @param {object} props - The props object
 * @param {string} props.title - The title of the question
 * @param {string} props.desc - The description of the question
 * @returns {JSX.Element}- The Question Card Component
 */
export function QuestionCard({title, desc}) {
    return (
        <div className="card my-3">
            <h5 className="card-header">
                <i className="fa fa-question-circle me-2"></i>
                Question
            </h5>
            <div className="card-body d-flex justify-content-between">
                <div className="card-body-content">
                    <h6 className="card-title">{title}</h6>
                    <p className="card-text">{desc}</p>
                </div>
            </div>
        </div>
    );
}
