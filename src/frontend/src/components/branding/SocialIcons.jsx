/**
 * Social Icons component.
 * @returns {JSX.Element}- The Social Icons Component
 */
export function SocialIcons() {
    return (
        <a href="https://gitlab.aber.ac.uk/jag88/cs22220_group1" className="d-flex justify-content-center position-absolute bottom-0 end-0 m-5">
            <i className="fab fa-gitlab fa-2x me-3"></i>
        </a>
    );
}