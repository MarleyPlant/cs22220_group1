/**
 * A Component to Display the Small Logo.
 * @param {object} props - The props object
 * @param {number} props.width - The width of the logo.
 * @param {number} props.height - The height of the logo.
 * @returns {JSX.Element}- The Small Logo Component
 */
export function HOGSmallLogo({width="auto", height=30}) {
    return <img
        src="/LogoSmall.png"
        height={height}
        width={width}
        alt=""
        loading="lazy"
        className="me-3"
    />;
}