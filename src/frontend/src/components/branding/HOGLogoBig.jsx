import React from "react";

/**
 * A Component to Display the Big Logo.
 * @returns {JSX.Element}- The Big Logo Component
 */
export function HOGBigLogo () {
    return <img alt="Big Logo" className="w-50" src="LogoMain.png"></img>;
}
