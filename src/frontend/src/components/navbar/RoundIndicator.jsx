import React from "react";
import { useGlobalState } from "../../app.js";

/**
 * A Component to Display Multiple Indicators.
 * @param {number} progress - The number of indicators to fill.
 * @param {number} progress.num - The number of indicators to display.
 * @returns {JSX.Element}- The Indicators Component
 */
function Indicators({progress, num}) {
    let indicators = Array(num).fill(0);

    return <div className="flex-row w-auto p-3 ms-2 d-flex">
        {indicators.map((_, index) => {
            return <Indicator key={index} fill={index < progress}/>;
        })}
    </div>;
}

/**
 * A Singular Indicator.
 * @param {boolean} props - A boolean indicating if the indicator should be filled.
 * @param {boolean} props.fill  - A boolean indicating if the indicator should be filled.
 * @returns {JSX.Element}- The Indicator Component
 */
function Indicator({fill}) {
    if (fill) {
        return <div className="rounded-circle bg-primary p-2 me-3"></div>;
    } else {
        return <div className="rounded-circle bg-secondary p-2 me-3"></div>;
    }
}

/**
 * A Component to Display All Rouind Indicators.
 * @returns {JSX.Element}- The Round Indicator Component
 */
export function RoundIndicator() {
    const [state] = useGlobalState();

    return (
        <div className="flex-row ms-5 h-auto d-flex align-items-center">
            <h6>Round:</h6>
            <Indicators progress={state.round} num={5}></Indicators>
        </div>
    );
}
