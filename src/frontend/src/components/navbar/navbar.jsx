import React from "react";
import {
    MDBNavbar,
    MDBContainer,
    MDBIcon,
    MDBNavbarNav,
    MDBNavbarItem,
    MDBNavbarLink,
    MDBNavbarBrand,
} from "mdb-react-ui-kit";
import { RoundIndicator } from "./RoundIndicator";
import { HOGSmallLogo } from "../branding/HOGSmallLogo";
import { Link } from "react-router-dom";

/**
 * Navbar Component.
 * @param {object} props - The props object.
 * @param {boolean} props.showround - Show round indicator
 * @param {string} props.roundType - Current round type
 * @returns {JSX.Element} The Navbar Component
 */
export function HOGNavbar({ showround = true, roundType = ""}) {
    return (
        <MDBNavbar light bgColor="light" className="p-3">
            <MDBContainer fluid>
                <Link to="/">
                    <MDBNavbarBrand tag="div">
                        <HOGSmallLogo />

                        <h2>House Of Games</h2>
                    </MDBNavbarBrand>
                </Link>
                {showround ? <RoundIndicator /> : ""}

                <MDBNavbarNav
                    right
                    className="flex-row align-items-center h-auto w-auto d-flex"
                >
                    {showround ? (
                        <div className="w-auto d-flex me-5 align-items-center h-auto">
                            <h5>{roundType}</h5>
                        </div>
                    ) : (
                        ""
                    )}
                    <MDBNavbarItem className="me-3 me-lg-2 align-items-center d-flex h-auto">
                        <MDBNavbarLink href="#">
                            <MDBIcon fas icon="sign-out" />
                        </MDBNavbarLink>
                    </MDBNavbarItem>
                </MDBNavbarNav>
            </MDBContainer>
        </MDBNavbar>
    );
}
