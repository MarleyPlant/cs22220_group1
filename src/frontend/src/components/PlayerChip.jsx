/**
 * A component for each player in the lobby.
 * @param {object} props - The props object
 * @param {object} props.player - Player object
 * @param {Function} props.remove - Function to remove player
 * @param {boolean} props.showRemove - Boolean for if the remove button should be shown
 * @param {string} props.className - Class names for styling
 * @returns {JSX.Element}- The Player Chip Component
 */
export function PlayerChip({player, remove, showRemove, className}) {
    return (
        <div className={"chip " + className} key={player.username}>
            {player.isHost ? <i className="fa fa-crown me-2"></i> : ""}
            {player.username}
            {showRemove && !player.isHost ? <i onClick={remove} className="fa fa-remove ms-3"></i> : "" }
        </div>
    );
}