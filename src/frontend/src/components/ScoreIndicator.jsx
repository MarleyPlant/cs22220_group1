import React from "react";
import { useGlobalState } from "../app";

/**
 * A component for displaying the score indicator.
 * @returns {any} The Score Indicator Component
 */
export function ScoreIndicator() {
    let [ state ] = useGlobalState();

    return (
        <div className="flex-row d-flex w-auto m-4 position-absolute bottom-0 end-0">
            <div className="leaderboard_position mx-2 bg-primary px-4 py-2 align-items-center d-flex h-auto w-auto rounded-pill">
                <h3 className="text-white">1st</h3>
            </div>
            <div className="leaderboard_position mx-2 bg-primary px-5 py-2 align-items-center d-flex h-auto w-auto rounded-pill">
                <h3 className="text-white">{ state.score }</h3>
            </div>
        </div>
    );
}
