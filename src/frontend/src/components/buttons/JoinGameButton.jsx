/**
 *
 * @param {object} props - The props object
 * @param {Function} props.onClick - Function to be called when button is pressed
 * @param {string} props.style - Style variant of button
 * @param {string} props.className - Name used for styling
 * @returns {JSX.Element}- The Join Game Button Component
 */
export function JoinGameButton({ onClick, style="primary", className="" }) {
    return (
        <button
            className={`btn btn-${style} ${className}`}
            role="button"
            type="button"
            onClick={onClick}
        >
            <i className="fa-solid mx-2 fa-gamepad" />
            Join Game
        </button>);
}