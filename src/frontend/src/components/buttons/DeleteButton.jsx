/**
 *
 * @param {object} props - The props object
 * @param {Function} props.onClick - Function to be called when button is clicked
 * @param {string} props.className - Additional class name
 * @returns {JSX.Element}- The Delete Button Component
 */
export function DeleteButton({ onClick, className = "" }) {
    return (
        <button onClick={onClick} className={`btn btn-danger ${className}`}>
            <i className="fa fa-trash me-2"></i>
            Delete
        </button>
    );
}