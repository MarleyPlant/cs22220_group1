import React from "react";
import { deleteQuestion } from "../api/questions";
import { deleteAnswer, getAnswer } from "../api/answers";
import { DeleteButton } from "./buttons/DeleteButton";
import { ALERTUSER } from "../util/global";

/**
 * A component for displaying a question card with options to edit or delete the question.
 * @param {object} props - The props object
 * @param {Function} props.update - Function to update the UI after deleting the question
 * @param {string} props.title - Question Title
 * @param {string} props.desc - Question Description
 * @param {string} props.id - Question ID
 * @returns {JSX.Element}- The Edit Question Card Component
 */
export function EditQuestionCard({ update, title, desc, id }) {
    return (
        <div className="card my-3">
            <h5 className="card-header">
                <i className="fa fa-question-circle me-2"></i>
                Question
            </h5>
            <div className="card-body d-flex justify-content-between">
                <div className="card-body-content">
                    <h6 className="card-title">{title}</h6>
                    <p className="card-text">{desc}</p>
                </div>
                <div className="card-actions gap-2 d-flex">
                    <a href={`/question/${id}`} className="btn btn-primary">
                        <i className="fa fa-edit me-2"></i>
                        Edit
                    </a>
                    <DeleteButton onClick={async () => {
                        await ALERTUSER(`DELETED ${title}`, "SUCCESS", () => { }, async () => {
                            let answerid = await getAnswer(id);
                            await deleteAnswer(answerid.id);
                            await deleteQuestion(id);
                            update();
                        });

                    }} />
                </div>
            </div>
        </div>
    );
}
