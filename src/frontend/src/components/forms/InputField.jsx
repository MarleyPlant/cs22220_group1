import React from "react";
import { MDBInput } from 'mdb-react-ui-kit';

/**
 * A reusable input field component.
 * @param {object} props - The props object
 * @param {string} props.label - Input field label
 * @param {string} props.type - The type of input field
 * @param {string} props.id - The ID for the input field
 * @param {string} props.defaultValue - The default value of the input field
 * @returns {JSX.Element}- The Input Field Component
 */
export function InputField({ label, type = "text", id, defaultValue }) {
    return (
        <MDBInput className="mb-3" label={label} id={id} type={type} defaultValue={defaultValue}/>
    );
}
