import React from "react";
import { InputField } from "../InputField.jsx";

/**
 *
 * @returns {JSX.Element}- The Username Field Component
 */
export function UsernameField() {
    return (
        <>
            <label htmlFor="#" className="form-label">
                Username:
            </label>
            <InputField type="text" className="form-control" id="name" required="" />
        </>
    );
}