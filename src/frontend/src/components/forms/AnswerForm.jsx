import React from "react";
import { InputField } from "./InputField";

/**
 * A reusable answer form component.
 * @param {object} props - The props object
 * @param {Function} props.onSubmit - The function to be called when submitted
 * @returns {JSX.Element} - The answer form component.
 */
export function AnswerForm({ onSubmit }) {

    const [answer, setAnswer] = React.useState(""); // Use React.useState to manage the answer state
    
    /**
     *
     * @param {object} event - The event object.
     */
    const handleChange = (event) => {
        setAnswer(event.target.value); // Update the answer state when the input value changes
    };

    /**
     *
     * @param {object} event - The event object.
     */
    const handleSubmit = (event) => {
        event.preventDefault(); // Prevent the form from causing a page reload
        if(onSubmit) {
            onSubmit(event.target[0].value);
        }
    };

    return (
        <form onSubmit={handleSubmit} className="w-100">
            <InputField
                label="Your Answer"
                type="text"
                id="questionAnswerForm"
                value={answer}
                onChange={handleChange}
                name="questionAnswerForm" // Add name attribute for easier retrieval (remove if not needed)
            />
            <button
                type="submit"
                className="btn btn-success btn-block btn-lg"
            >
                <i className="fa fa-check-to-slot me-2"></i> {/* Fix self-closing tag */}
                Submit Answer
            </button>
        </form>
    );
}
