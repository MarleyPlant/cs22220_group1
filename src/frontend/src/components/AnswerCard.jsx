import React from "react";

/**
 * A component to display an answer card
 * @param {object} props - The props object
 * @param {string} props.desc - The description of the card
 * @returns {JSX.Element}The Answer Card Component
 */
export function AnswerCard({desc}) {
    return (
        <div className="card w-50">
            <div className="card-body d-flex justify-content-between">
                <div className="card-body">
                    <h6 className="card-title">Correct Answer</h6>
                    <p className="card-text">{desc}</p>
                </div>
            </div>
        </div>
    );
}
