import React from "react";
import { updateSession } from "../api/sessions";
import { nextQuestion } from "../util/rounds/navigation";

/**
 * A component for displaying game controls such as skip and end game.
 * @param {object} props - The props object
 * @param {object} props.session - Session object
 * @param {object} props.state - The state object
 * @param {Function} props.dispatch - Functiuon for updating state
 * @returns {JSX.Element}- The Game Controls Component
 */
export function GameControls({ session, state, dispatch }) {
    /**
     * End current game
     */
    async function endgame() {
        // await deleteSession(id);
        session.hasEnded = true;
        await updateSession(session);
    }

    /**
     * Skip to the next question
     */
    async function skipRound() {
        // Increment the round counter
        session.currentRound++;
        // Move to the next question
        await nextQuestion(session.id, state, dispatch);
    }

    if (session.hostID == state.playerid) {
        return (
            <div className="flex-row d-flex w-auto m-4 position-absolute bottom-0 start-0">
                <button onClick={endgame} className=" bg-danger px-4 py-2 align-items-center d-flex h-auto w-auto rounded-pill">
                    <h3 className="text-white"><i className="fa-solid fa-x"></i> End Game</h3>
                </button>
                <button onClick={skipRound} className=" bg-warning px-4 py-2 align-items-center d-flex h-auto w-auto rounded-pill">
                    <h3 className="text-white"><i className="fa-solid fa-forward"></i> Skip</h3>
                </button>
            </div>
        );
    }

    return null;
}
