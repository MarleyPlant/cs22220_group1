import React from "react";
import { HOGDefaultLayout } from "./LayoutDefault";
import { GameControls } from "../components";

/**
 * A default layout for the site.
 * @param {object} props - The props object
 * @param {object} props.children - The children of the layout
 * @param {boolean} props.showScore - Boolean for whether to show score indicator
 * @param {boolean} props.showRound - Boolean for whether to show round indicator
 * @param {string} props.roundType - Type of round currently being played
 * @param {object} props.session - The session object for the game
 * @param {object} props.state - the state object
 * @param {Function} props.dispatch - Function for updating state
 * @returns {JSX.Element} - The Game Page Layout
 */
export function GamePageLayout({
    children,
    showScore = true,
    showRound = true,
    roundType = "",
    session,
    state,
    dispatch
}) {

    return (
        <HOGDefaultLayout roundType={roundType} showround={showRound} showScore={showScore}>
            <GameControls session={session} state={state} dispatch={dispatch}></GameControls>
            {children}
        </HOGDefaultLayout>
    );
}