import React from "react";
import { HOGNavbar, ScoreIndicator } from "../components";

/**
 *
 * @param {object} props - The props object
 * @param {object} props.children - The children of the layout
 * @param {boolean} props.showScore - Boolean for whether to show score indicator
 * @param {boolean} props.showRound - Boolean for whether to show round indicator
 * @param {string} props.roundType - Type of round currently being played
 * @returns {JSX.Element} - The Default Layout
 */
export function HOGDefaultLayout({
    children,
    showScore = true,
    showRound = true,
    roundType = "",
}) {
    return (
        <div className="h-100">
            <HOGNavbar roundType={roundType} showround={showRound} showScore={showScore}></HOGNavbar>
            {showScore ? <ScoreIndicator /> : ""}
            <div className="w-80  align-items-center flex-column d-flex justify-content-center min-vh-90">
                {children}
            </div>
        </div>
    );
}