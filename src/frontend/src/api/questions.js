import { generateEndPointURL, handleAPIResponse } from "./";

/**
 * Create New Question.
 * @param {string} text - The Question Text.
 * @param {string} type - The Question Type.
 * @param {string} image - The Image Path.
 * @returns {Promise} Response From API.
 */
export async function createQuestion(text, type, image) {
    let response = await fetch(generateEndPointURL("questions/add.php", {
        questionText: text,
        questionType: type,
        imagePath: image
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}


/**
 * Get All Questions.
 * @returns {Promise} All Questions.
 */
export async function getQuestions() {
    let response = await fetch(generateEndPointURL("questions/index.php"), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}

/**
 * Update Question.
 * @param {string} id - The Question ID.
 * @param {string} text - The Question Text.
 * @returns {Promise} Response From API.
 */
export async function updateQuestion(id, text) {
    let response = await fetch(generateEndPointURL("questions/update.php", {
        id: id,
        questionText: text
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}

/**
 * Get Question By ID.
 * @param {string} id - The Question ID.
 * @returns {Promise} The Question Data.
 */
export async function getQuestion(id) {
    let response = await fetch(generateEndPointURL("questions/index.php", { 
        id: id
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}

/**
 * Delete Question.
 * @param {string} id - The Question ID.
 * @returns {Promise} Response From API.
 */
export async function deleteQuestion(id) {
    let response = await fetch(generateEndPointURL("questions/delete.php", {
        id: id
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}