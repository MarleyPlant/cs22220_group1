import { generateEndPointURL } from './';
import { handleAPIResponse } from './';

/**
 * Gets an answer from the database using the ID.
 * @param {string} id - The ID of the answer.
 * @returns {Promise} - The answer object.
 */
export async function getAnswer(id) {
    let url = generateEndPointURL("answers/index.php", { questionID: id });
    let response = await fetch(url, {
        method: 'GET'
    });
    return await handleAPIResponse(response);
}

/**
 * Updates an answer on the database using the ID.
 * @param {string} id - The ID of the answer to update.
 * @param {string} data - The updated data for the answer.
 * @returns {Promise} - The response from the server.
 */
export async function updateAnswer(id, data) {
    let url = generateEndPointURL("answers/update.php", { id, answerText: data });
    let response = await fetch(url, {
        method: 'GET',
    });
    return await handleAPIResponse(response);
}

/**
 * Delete an answer from the database.
 * @param {string} id - The ID of the answer to delete.
 * @returns {Promise} - The response from the server.
 */
export async function deleteAnswer(id) {
    let url = generateEndPointURL("answers/delete.php", { id });
    let response = await fetch(url, {
        method: 'POST'
    });

    try {
        return await handleAPIResponse(response);
    } catch (e) {
        console.log(e);
    }
}

/**
 * Creates a new answer in the database.
 * @param {string} id - The ID of the question linked to the new answer.
 * @param {string} text - The text of the new answer.
 * @returns {Promise} - The response from the server.
 */
export async function createAnswer(id, text) {
    let url = generateEndPointURL("answers/add.php", { answerText: text, questionID: id });
    let response = await fetch(url, {
        method: 'GET',
    });
    return await handleAPIResponse(response);
}