import { getSession } from "./sessions";
import { generateEndPointURL, handleAPIResponse } from "./";

/**
 * Add New Player To The Database.
 * @param {string} username - The Player's Username.
 * @param {object} state - The Global State.
 * @param {Function} dispatch - The Global State Dispatcher.
 * @returns {Promise} The Player's ID.
 */
export async function addPlayer(username, state, dispatch) {
    if (state.playerid == undefined) {
        let url = generateEndPointURL("players/add.php", { username });
        let response = await fetch(url, {
            method: 'GET'
        });

        let playerID = await response.json();
        playerID = parseInt(playerID);

        await dispatch({ playerid: playerID, sessionid: state.sessionid });
        return playerID;
    } else {
        return state.playerid;
    }
}

/**
 * Get Player From Database.
 * @param {number} id - The Player's ID.
 * @param {string} session - The Session ID.
 * @returns {Promise} The Player Data.
 */
export async function getPlayer(id, session=null) {
    let url = generateEndPointURL("players/index.php", { id });
    let response = await fetch(url, {
        method: 'GET'
    });

    let data = await response.json();

    if(session != null) {
        let sessiondata = await getSession(session);
        data.isHost = sessiondata.hostID == id;   
    }

    return await data;
}

/**
 * Connect Player To Session.
 * @param {number} id - The Player's ID.
 * @param {string} session  - The Session ID.
 * @returns {Promise} Response From API.
 */
export async function connectPlayer(id, session) {
    let url = generateEndPointURL("players/connect.php", { player: id, session });
    let response = await fetch(url, {
        method: 'GET'
    });

    return await handleAPIResponse(response);
}

/**
 * Disconnect Player From Session.
 * @param {number} id - The Player's ID.
 * @param {string} session - The Session ID.
 * @returns {Promise} Response From API.
 */
export async function disconnectPlayer(id, session) {
    let url = generateEndPointURL("players/disconnect.php", { player: id, session });
    let response = await fetch(url, {
        method: 'GET'
    });

    return await handleAPIResponse(response);
}