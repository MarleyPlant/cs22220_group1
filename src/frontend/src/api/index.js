import { DEBUGLOG } from '../util/global';
import { BASEURL, DEBUG } from '../config';

/**
 * Handle API Response.
 * @param {object} response - Response From API.
 * @returns {object} - Data From API.
 */
export async function handleAPIResponse(response) {
    let data;
    let temp;

    // IF Server Is Not Responding, Display an Error.

    // IF Response Is Not OK, Display an Error.
    if (!response.ok) {
        console.error(response);
        return {};
    }

    // Attempt To Parse Response
    temp = await response.text();

    try {
        data = await JSON.parse(temp);
    } catch (e) {
        // IF Object Cannot Be Parsed, Return Empty Object
        DEBUGLOG("Error Parsing Response", "ERROR", { code: e.toString(), response: temp });
        return [];
    }

    if (data.error) {
        DEBUGLOG("Error On Backend", "ERROR", data);
        return [];
    }

    if (DEBUG.includes("RESPONSE")) {
        console.log("API Response: ")
        console.table(data);
        console.log(" ")
    }


    return data;
} 

/**
 * Generate End Point URL For API.
 * @param {string} resource - Resource To Access.
 * @param {object} parameters - Parameters To Pass.
 * @returns {string} The Generated URL.
 */
export function generateEndPointURL(resource, parameters) {
    // Remove Null Or Undefined Parameters
    for (let key in parameters) {
        if (parameters[key] === null || parameters[key] === undefined) {
            delete parameters[key];
        }
    }

    // Convert Parameters To URL Encoded String
    let params = new URLSearchParams(parameters);
    let urlparamdata = '';
    if(params.size > 0) {
        urlparamdata += '?';
    }
    urlparamdata +=`${params.toString()}`;

    // Generate URL
    let url = `${BASEURL}/${resource}${urlparamdata}`

    DEBUGLOG(url, "API")

    return url;
}

export * from './answers';
export * from './questions';
export * from './sessions';
export * from './scores';
export * from './players';
