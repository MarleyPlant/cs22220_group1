import { generateEndPointURL, handleAPIResponse } from './';

/**
 * Increments the score by 1 and dispatches the new state to the global state.
 * @param {string} sessionID - The ID of the session
 * @param {object} state - Global state
 * @param {Function} dispatch - Global state dispatcher
 */
export async function incrementScore(sessionID, state, dispatch) {
    // Increment score by 1
    state.score += 1;
    // Add the updated score to the database
    let score = await getScore(sessionID, state.playerid);

    if(score.length === 0) {
        await addScore(sessionID, state.playerid, state.score);
    } else {
        await updateScore(sessionID, state.playerid, state.score);
    }

    dispatch(state);
}


/**
 * Get all scores for a session.
 * @param {string} sessionID - The ID of the session
 * @returns {Promise} A Promise that resolves to an array containing all scores for the session
 */
export async function getAllscores(sessionID) {
    let response = await fetch(generateEndPointURL("scores/index.php", {
        sessionID: sessionID
    }), {
        method: 'GET'

    });
    
    return await handleAPIResponse(response);

}


/**
 * Add a score for a player in a session.
 * @param {string} sessionID - The ID of the session
 * @param {string} playerID - The ID of the player
 * @returns {Promise} A Promise that gets response from the API
 */export async function getScore(sessionID, playerID) {
    let response = await fetch(generateEndPointURL("scores/index.php", {
        sessionID: sessionID,
        playerID: playerID
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}

/**
 * Add A New Score entry To the the database.
 * @param {string} sessionID - The ID of the session
 * @param {string} playerID - The ID of the player
 * @param {number} score - The score to add
 * @returns {Promise} A Promise that gets response from the API
 */
export async function addScore(sessionID, playerID, score) {
    let response = await fetch(generateEndPointURL("scores/add.php", {
        sessionID: sessionID,
        playerID: playerID,
        score: score
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}

/**
 * Update a score entry in the database.
 * @param {string} sessionID - The ID of the session
 * @param {string} playerID - The ID of the player
 * @param {number} score - The score to add
 * @returns {Promise} A Promise that gets response from the API
 */
export async function updateScore(sessionID, playerID, score) {
    let response = await fetch(generateEndPointURL("scores/update.php", {
        sessionID: sessionID,
        playerID: playerID,
        score: score
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}