import { generateEndPointURL, handleAPIResponse } from "./";
import { DEBUGLOG } from "../util/global.js";
import { connectPlayer } from "./players.js";

/**
 * Create New Session.
 * @param {string} playerID - Player ID.
 * @param {Function} navigate - Navigation Function.
 */
export function createNewSession(playerID, navigate) {
    // Make a GET request
    fetch(generateEndPointURL("sessions/add.php", {
        hostID: playerID,
    }), {
        method: 'GET'
    })
        .then(response => {
            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            return response.json();
        })
        .then(async data => {
            // Simulate an HTTP redirect:
            await connectPlayer(playerID, data['gameid']);
            navigate("/lobby/" + data['gameid']);
        })
        .catch(error => {
            console.error('Error:', error);
        });
}


/**
 * Get All Sessions.
 * @returns {Array} All Sessions.
 */
export async function getSessions() {
    let response = await fetch(generateEndPointURL("sessions/index.php", {}), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}

/**
 * Delete Session.
 * @param {string} sessionID - Session ID.
 * @returns {Promise} Response From API.
 */
export async function deleteSession(sessionID) {
    let response = await fetch(generateEndPointURL("sessions/delete.php", {
        id: sessionID
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}


/**
 * Get Session.
 * @param {string} id - Session ID.
 * @returns {Promise} Session Data.
 */
export async function getSession(id) {
    return fetch(generateEndPointURL("sessions/", {
        id: id
    }), {
        method: 'GET'
    }).then(async response => {
        if (!response.ok) {
            throw new Error('Network response was not ok');
        }
        return await handleAPIResponse(response);
    });
}

/**
 * Update Session.
 * @param {object} session - Session Data.
 * @returns {Promise<object>} Response From API.
 */
export async function updateSession(session) {
    if(session.isAnswering == null){
        session.isAnswering = "NULL";
    }
    
    let response = await fetch(generateEndPointURL("sessions/update.php", {
        id: session.id,
        currentRound: session.currentRound,
        currentQuestion: session.currentQuestion,
        noOfConnectedUsers: session.noOfConnectedUsers,
        isAnswering: session.isAnswering,
        hasEnded: session.hasEnded
    }), {
        method: 'GET'
    })

    if(response.ok){
        return await handleAPIResponse(response);
    } else {
        DEBUGLOG("Failed to update session", "ERROR", response.text());
    }
}

/**
 * Add A Question that has already occured to the list of already occured questions in the database
 * @param {string} sessionID - Session ID
 * @param {string} questionID - Question ID
 * @returns {Promise} A Promise that gets response from the API
 */
export async function addAnsweredQuestionToSession(sessionID, questionID){
    let response = await fetch(generateEndPointURL("sessions/addAnsweredQuestion.php", {
        sessionID: sessionID,
        questionID: questionID,
    }), {
        method: 'GET'
    })
    return await handleAPIResponse(response);
}